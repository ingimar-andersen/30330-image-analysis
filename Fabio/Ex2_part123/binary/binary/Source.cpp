#include <iostream>
#include <opencv.hpp>
#include <highgui.h>
#include <stdio.h>
#include <math.h>

using namespace std;

IplImage* hist(IplImage*img);
IplImage* greytobin(IplImage*img);
IplImage* moments(IplImage*img,IplImage*bin2);
IplImage* Path(string path)
{
	return nullptr;
}

IplImage* hist(IplImage*img) {
	//Create the histogram of a greyscale image
	int w = 750;
	int h = 500;
	IplImage*output;
	int row, col, histogram[256] = {};
	uchar b;
	for (int k = 0; k <= 255; k++) {
		histogram[k] = 0;
	}

	for (row = 0; row < img->height; row++)
	{
		for (col = 0; col < img->width; col++)
		{
			b = CV_IMAGE_ELEM(img, uchar, row, col);
			//g = CV_IMAGE_ELEM(img, uchar, row, col * 3 + 1);
			//r = CV_IMAGE_ELEM(img, uchar, row, col * 3 + 2);
			histogram[b] += 1;
		}
	}
	output = cvCreateImage(cvSize(w, h), 8, 3);
	cvNamedWindow("Histogram", CV_WINDOW_AUTOSIZE);
	for (int k = 0; k <= 255; k++)
	{
		cvRectangle(output, cvPoint(k*w / 256, h), cvPoint((k*w / 256), h - histogram[k] / 12), CV_RGB(0, 125, 0), 1);
	}
	return output;
}
IplImage* greytobin(IplImage*img) {
	//This function makes the copy image binary by thresholding 
	string t;
	cout << "Define thresold parameter: " << endl;
	cin >> t;
	unsigned int t1;
	stringstream(t) >> t1;
	
	int w = 750;
	int h = 500;
	int pos;
	unsigned int np = 0;
	uchar pint;

	for (pos = 0; pos < img->height*img->width; pos++)
	{
		//access pixel intensity and modify it
		pint = img->imageData[pos];
		if (pint >= 255-t1) {
			img->imageData[pos] = 255;
		}
		else {
			img->imageData[pos] = 0;
		}
	}
	return img;
}
IplImage* moments(IplImage*img, IplImage*bin2) {
	//This function computes moments, center of mass and orientation
	//of the given binary image. Finally It add some features to the
	//original picture.
	int pos, x = 0, y = 0; //pixels coordinates
	int M10 = 0, M01 = 0, M11 = 0, M20 = 0, M02 = 0; //moments
	float mu11 = 0, mu02 = 0, mu20 = 0, th; //central moments
	int Area = 0, XCM, YCM; //Area and center of mass

	for (pos = 0; pos < bin2->width*bin2->height; pos++) {
		y = pos / bin2->width;
		x = pos - y*bin2->width;
		if (bin2->imageData[pos] == 0) {
			M10 = M10 + x;
			M01 = M01 + y;
			M11 = M11 + x*y;
			M20 = M20 + x*x;
			M02 = M02 + y*y;
			Area = Area + 1;
		}
	}
	XCM = M10 / Area;
	YCM = M01 / Area;
	mu11 = (M11 - XCM*M01) / Area ^ 2;
	mu20 = (M20 - XCM*M10) / Area ^ 2;
	mu02 = (M02 - YCM*M01) / Area ^ 2;

	th = 0.5*atan2(2 * mu11, mu20 - mu02);
	
	cout << "The image orientation is: " << th << endl;
	cout << "The center of mass has coordinates: " << XCM << " ; " << YCM << endl;

	cvLine(img, cvPoint(XCM - 20, YCM), cvPoint(XCM + 20, YCM), CV_RGB(255, 255, 255), 2);
	cvLine(img, cvPoint(XCM, YCM - 20), cvPoint(XCM, YCM + 20), CV_RGB(255, 255, 255), 2);
	cvLine(img, cvPoint(0, tan(th)*(0 - XCM) + YCM), cvPoint(img->width, tan(th)*(img->width - XCM) + YCM), CV_RGB(255, 255, 255), 2);

	return img;
}
IplImage* Path() {
	//This function take a path string as input and load
	//the chosen image.
	string path;

	cout << "Input a path file" << endl;
	cin >> path;

	const char *c = path.c_str();

	//Image Loading from Path:
	IplImage* img = cvLoadImage(c, IPL_DEPTH_8U);
	cvNamedWindow("Image", CV_WINDOW_AUTOSIZE);

	return img;
}


int main()
{
	//show the original image and its histogram
	IplImage *img = Path();
	cvShowImage("Histogram", hist(img));

	//Create copy of the image
	IplImage *bin  = cvCreateImage(cvSize(img->width, img->height), IPL_DEPTH_8U,1);
	cvCopy(img, bin);
	
	//Make it binary
	IplImage* bin2 = greytobin(bin);
	
	//Compute the moments and the center of mass
	IplImage*img2 = moments(img, bin2);
	
	//Display the binary image and the original one with the new features
	cvNamedWindow("binary", CV_WINDOW_AUTOSIZE);
	cvShowImage("binary", bin2);
	cvShowImage("Image", img2);
	
	//Press any keys to shut down the windows:
	cvWaitKey(0);
	
	//Destroy the windows
	cvDestroyWindow("Image");
	cvDestroyWindow("Histogram");
	cvDestroyWindow("binary");

	return 0;
}