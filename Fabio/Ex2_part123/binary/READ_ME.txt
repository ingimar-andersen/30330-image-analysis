The thresholding value is actually 149 for the pen example. It corresponds to 100 in Ingimar code. This because my reference zero is 255 instead of 0.

I do not compute the invariant moments just for lack of time. But it should be quite straightforward. 

All the functions are declared inside the main code. Next step will be to collect them in a header.