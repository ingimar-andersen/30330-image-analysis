﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu;
using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure; //include structure database
using Emgu.CV.CvEnum;

namespace Ex_4
{
    class Program
    {
        static void Main(string[] args)
        { 

            string filename = "D:\\Applications\\PIC1_L.png";
            string filename1 = "D:\\Applications\\PIC1_R.png";

            Image<Gray, byte> img = new Image<Gray, byte>(filename);
            Image<Gray, float> img2 = new Image<Gray, float>(img.Size);
            Image<Gray, byte> img3 = new Image<Gray, byte>(filename1);
            Image<Gray, float> img4 = new Image<Gray, float>(img.Size);
            Image<Gray, byte> img5 = img.Copy();
            Image<Gray, byte> img6 = img3.Copy();
            Image<Gray, byte> img7 = new Image<Gray, byte>(2*img.Width,2*img.Height);
            Image<Gray, byte> img8 = img.Copy();
            CvInvoke.CornerHarris(img, img2, 2);
            CvInvoke.Normalize(img2, img2, 255, 0, NormType.MinMax, DepthType.Cv32F);
            CvInvoke.CornerHarris(img3, img4, 2);
            CvInvoke.Normalize(img4, img4, 255, 0, NormType.MinMax, DepthType.Cv32F);

            int n = 1;
            double min = 0;
            double max = 255;
            int count1 = 0;
            int[,] loc = new int[500, 2];
            int[,] loc2 = new int[500, 2];
            int[,] loc3 = new int[500, 2];
            int[,] loc4 = new int[500, 2];

            for (int k = n; k < img2.Width - n; k= k +2*n+1)
            {
                for (int i = n; i < img2.Height - n; i = i + 2 * n + 1)
                {
                    img2.ROI = new Rectangle(k, i, 2 * n + 1, 2 * n + 1);
                    Point minp = new Point(k - n, i - n);
                    Point maxp = new Point(k + n, i + n);
                    CvInvoke.MinMaxLoc(img2, ref min, ref max, ref minp, ref maxp);

                    if (max > 107)
                    {
                        maxp.X = maxp.X + k;
                        loc[count1, 0] = maxp.X;
                        maxp.Y = maxp.Y + i;
                        loc[count1, 1] = maxp.Y;
                        if (count1 > 0 && loc[count1, 0] == loc[count1 - 1, 0])
                        {
                            Array.Clear(loc, 2 * (count1), 2);
                        }
                        else if (count1 > 0 && loc[count1, 1] == loc[count1 - 1, 1])
                        {
                            Array.Clear(loc, 2 * (count1), 2);
                        }
                        else
                        {
                            count1++;
                        }
                    }
                    CvInvoke.cvResetImageROI(img2);
                }
            }
            Console.WriteLine(count1);
            int count2 = 0;
            for (int k = n; k < img.Width - n; k = k + 2 * n + 1)
            {
                for (int i = n; i < img.Height - n; i = i + 2 * n + 1)
                {
                    img4.ROI = new Rectangle(k, i, 2 * n + 1, 2 * n + 1);
                    Point minp = new Point(k - n, i - n);
                    Point maxp = new Point(k + n, i + n);
                    CvInvoke.MinMaxLoc(img4, ref min, ref max, ref minp, ref maxp);

                    if (max > 60)
                    {
                        maxp.X = maxp.X + k;
                        loc2[count2, 0] = maxp.X;
                        maxp.Y = maxp.Y + i;
                        loc2[count2, 1] = maxp.Y;
                        if (count2 > 0 && loc2[count2, 0] == loc2[count2 - 1, 0])
                        {
                            Array.Clear(loc2, 2 * (count2), 2);
                        }
                        else if (count2 > 0 && loc2[count2, 1] == loc2[count2 - 1, 1])
                        {
                            Array.Clear(loc2, 2 * (count1), 2);
                        }
                        else
                        {
                            count2++;
                        }
                    }
                    CvInvoke.cvResetImageROI(img4);
                }
            }
            Console.WriteLine(count2);
            int m2 = 0;
            int m2_old = int.MaxValue;
            int indfinal = 0;
            int mnew = 0;
            
            n = 5;
            int count = 0;
            for (int i = 0; i < 500; i++)
            {
                if (loc[i, 0] >= n && loc[i, 1] >= n && loc[i, 0] <= img.Width - n && loc[i, 1] <= img.Height - n)
                {
                    img5.ROI = new Rectangle(loc[i, 0], loc[i, 1], 2 * n + 1, 2 * n + 1);

                    for (int k = 0; k < 500; k++)
                    {
                        m2 = 0;
                        if (loc2[k, 0] >= n && loc2[k, 1] >= n && loc2[k, 0] <= img.Width - n && loc2[k, 1] <= img.Height - n)
                        {
                            
                            img6.ROI = new Rectangle(loc2[k, 0], loc2[k, 1], 2 * n + 1, 2 * n + 1);

                            for (int l = -n; l <= n; l++)
                            {
                                for (int m = -n; m <= n; m++)
                                {
                                    mnew = (img.Data[img5.ROI.Y+l, img5.ROI.X + m,0] - img3.Data[img6.ROI.Y + l, img6.ROI.X + m, 0]);
                                    m2 = m2 + Math.Abs(mnew);//* mnew;
                                }
                            }

                            if (m2 < m2_old)
                            {
                                m2_old = m2;
                                indfinal = k; 
                            }

                            CvInvoke.cvResetImageROI(img6);
                        }
                        
                    }
                    CvInvoke.cvResetImageROI(img5);
                    

                    if (Math.Abs(loc2[indfinal, 1] - loc[i, 1]) < 10 && Math.Abs(loc2[indfinal, 1] - loc[i, 1]) > 2)
                    {
                        loc3[count, 0] = loc[i, 0];
                        loc3[count, 1] = loc[i, 1];
                        loc4[count, 0] = loc2[indfinal, 0];
                        loc4[count, 1] = loc2[indfinal, 1];
                        Console.WriteLine(loc3[count, 0]);
                        CvInvoke.Circle(img, new Point(loc3[count, 0], loc3[count, 1]), 1, new MCvScalar(0, 0), 2);
                        CvInvoke.PutText(img, count.ToString(), new Point(loc3[count, 0], loc3[count, 1]), FontFace.HersheyComplex, 0.6, new Gray(0).MCvScalar);
                        CvInvoke.Circle(img3, new Point(loc4[count, 0], loc4[count, 1]), 1, new MCvScalar(0, 0), 2);
                        CvInvoke.PutText(img3, count.ToString(), new Point(loc4[count, 0], loc4[count, 1]), FontFace.HersheyComplex, 0.6, new Gray(0).MCvScalar);
                        count++;
                    }
                    m2_old = int.MaxValue;
                }
                
            }

            double x1 = loc3[0, 0];
            double x2 = loc3[5, 0];
            double x3 = loc3[9, 0];
            double y1 = loc3[0, 1];
            double y2 = loc3[5, 1];
            double y3 = loc3[9, 1];

            double den = x1 * y2 - x2 * y1 - x1 * y3 + x3 * y1 + x2 * y3 - x3 * y2;

            double[,] PM = new double[3, 3]
            {
             {(x2*y3 - x3*y2)/den,-(x1*y3 - x3*y1)/den, (x1*y2 - x2*y1)/den},
             { (y2 - y3)/den,-(y1 - y3)/den,(y1 - y2)/den},
             {-(x2 - x3)/den, (x1 - x3)/den,-(x1 - x2)/den}
            };

            double[,] tn1 = new double[3, 1]
            {
                {loc4[0,0] },
                {loc4[5,0] },
                {loc4[9,0] }
            };

            double[,] tn2 = new double[3, 1] {
                {loc4[0,1] },
                {loc4[5,1] },
                {loc4[9,1] }
            };
            //var PM1 = new Matrix<double>(PM);
            //var tn11 = new Matrix<double>(tn1);
            double[] p = new double[3];
            double[] q = new double[3];
            double sum = 0;
            double sum1 = 0;

            for (int i = 0; i < 3; i++)
            {
                for (int k = 0; k < 3; k++)
                {
                    sum = sum + PM[i, k] * tn1[k,0];
                    sum1 = sum1 + PM[i, k] * tn2[k, 0];
                }
                p[i] = sum;
                //Console.WriteLine(p[i]);
                q[i] = sum1;
                sum = 0;
                sum1 = 0;
            }
            
            for (int i = 0; i < img.Height; i++)
            {
                for (int k = 0; k < img.Width; k++)
                {
                    var x_p = p[0] + p[1] * k + p[2] * i;
                    var y_p = q[0] + q[1] * k + q[2] * i;

                    var y_p1 = Convert.ToInt32(y_p);
                    var x_p1 = Convert.ToInt32(x_p);
                    //x_p1 < img7.Width && y_p1 < img7.Height &&
                    //Console.WriteLine(x_p1);
                    if (x_p1>=0 && y_p1 >=0)
                    {

                        //Console.WriteLine(img.Data[i, k, 0]);
                        //Console.WriteLine(x_p1);
                        img7.Data[y_p1, x_p1, 0] = img8.Data[i, k, 0];
                    }
                }
            }

            CvInvoke.Imshow("LEFT", img);
            CvInvoke.Imshow("RIGHT", img3);
            CvInvoke.Imshow("CORRESPONDANCE", img7);

            CvInvoke.WaitKey(0);
        }
    }
}

