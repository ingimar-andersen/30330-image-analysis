# README #
Exercises and project work from the course [Image Analysis with Microcomputer](http://kurser.dtu.dk/course/30330) at the Technical University of Denmark.

### Project description ###
An image analysis software system was developed, that could assist with an autonomous landing on Jupiter´s moon, Europa. The system can estimate the attitude and position of a spacecraft, given the distance to the surface and a camera video feed. The video feed has been artificially generated from high-resolution images of the surface of Europa. 

The project work is found under 'Exercises/Europa'.

### Before you commit ###

ONLY USE Visual Studio Community 2015 if you want to commit changes to the repository. The solutions will only run on VS 2015 or later.

### FOR C# PROJECTS ###

The Emgu CV folder should be located under:
C:\Externals\Emgu.CV.3.2.0.2721


### FOR C++ PROJECTS ###

The opencv folder should be located under:
C:\Externals\opencv2.4.9

If opencv is installed choose to add the folder to the PATH environment variable during installation.

If the folder is copy-pasted add the folder "C:\Externals\opencv2.4.9\build\x86\vc12\bin"
manually through Control panel -> System -> Advanced system settings -> Environment Variables...
