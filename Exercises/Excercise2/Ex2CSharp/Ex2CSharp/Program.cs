﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Excercise1CSharp
{
    class Program
    {
        public const string EXIT_COMMAND = "exit";

        private enum InputCommand
        {
            Exit = 0,
            ImageMoments = 1,
            MomentInvariances = 2,
        }

        static void Main(string[] args)
        {
            InputCommand inputCommand;
            TryReadInputCommand(out inputCommand);

            switch (inputCommand)
            {
                case InputCommand.Exit:
                    return;
                case InputCommand.ImageMoments:
                    ImageMoments();
                    break;
                case InputCommand.MomentInvariances:
                    MomentInvariances();
                    break;
                default:
                    return;
            }
        }

        private static void MomentInvariances()
        {
            var image1 = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise2\\Ex2CSharp\\Ex2CSharp\\WalletStraightScaledDown.png", Emgu.CV.CvEnum.ImreadModes.AnyColor);
            //var image1 = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise2\\Ex2CSharp\\Ex2CSharp\\PEN.pgm", Emgu.CV.CvEnum.ImreadModes.AnyColor);
            var image2 = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise2\\Ex2CSharp\\Ex2CSharp\\WalletRotated.png", Emgu.CV.CvEnum.ImreadModes.AnyColor);
            string input = "";
            int threshold = 0; // 100 is the magic number
            ReadInputThreshold(out input, out threshold);
            while (input.ToLower() != EXIT_COMMAND)
            {
                //Thread that handles the image processing. The main thread is reserved for user input.
                new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;

                    //Image 1
                    var binaryImage1 = ImageUtils.CreateBinaryImage(image1, threshold);
                    var huInvariantMoment1 = ImageUtils.CalcHuInvariantMoments(binaryImage1.BinaryImage, binaryImage1.CenterOfMass);

                    DrawingUtils.DrawHeaderText(String.Format("HU: {0:E2}", huInvariantMoment1), binaryImage1.BinaryImage);

                    var windowName1 = String.Format("BI 1 {0}", threshold);
                    CvInvoke.Imshow(windowName1, binaryImage1.BinaryImage);


                    //Image 2
                    var binaryImage2 = ImageUtils.CreateBinaryImage(image2, threshold);
                    var huInvariantMoment2 = ImageUtils.CalcHuInvariantMoments(binaryImage2.BinaryImage, binaryImage2.CenterOfMass);

                    var percentageMatch = Math.Min(huInvariantMoment1, huInvariantMoment2) / Math.Max(huInvariantMoment1, huInvariantMoment2);
                    DrawingUtils.DrawHeaderText(String.Format("HU: {0:E2}, Match: {1:P}", huInvariantMoment2, percentageMatch), binaryImage2.BinaryImage);

                    var windowName2 = String.Format("BI 2 {0}", threshold);
                    CvInvoke.Imshow(windowName2, binaryImage2.BinaryImage);

                    CvInvoke.WaitKey(-1); //Just to keep the thread alive. The thread is killed once all windows are closed.

                    binaryImage1.Dispose();
                    binaryImage2.Dispose();

                }).Start();

                ReadInputThreshold(out input, out threshold);
            }

        }

        private static void ImageMoments()
        {
            try
            {
                string input = "";
                int threshold = 0;
                while (true)
                {
                    ReadInputThreshold(out input, out threshold);
                    if(input == EXIT_COMMAND)
                    {
                        return;
                    }
                    GenerateImageMomentWindows(threshold);
                }
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }

        private static void GenerateImageMomentWindows(int threshold)
        {
            new Thread(() => {
                //image = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise2\\Ex2CSharp\\Ex2CSharp\\PEN.pgm", Emgu.CV.CvEnum.ImreadModes.AnyColor);
                var image = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise2\\Ex2CSharp\\Ex2CSharp\\WalletStraight.png", Emgu.CV.CvEnum.ImreadModes.AnyColor);
                //image = CvInvoke.Imread("C:\\Users\\grena\\OneDrive\\DTU\\2017 - Haust\\30330 Image Analysis with Microcomputer\\TestImages\\WalletRotated.png", Emgu.CV.CvEnum.ImreadModes.AnyColor);
                CvInvoke.Imshow("Original", image);

                using (var binaryImage = ImageUtils.CreateBinaryImage(image, threshold))
                {
                    CvInvoke.Imshow(String.Format("BI {0}", threshold), binaryImage.BinaryImage);

                    using (var hist = ImageUtils.GenerateHistogram(image))
                    {
                        CvInvoke.Imshow("Histogram", hist);
                        CvInvoke.WaitKey(-1);
                    }
                }

                    
            }).Start();
        }

        private static void TryReadInputCommand(out InputCommand input)
        {
            Console.WriteLine("Enter '1' for Part 3: Image moments or \n'2' for Part 4: Moment invariances");
            string inputString = Console.ReadLine();
            inputString = inputString.Trim();
            int inputInt;
            if (int.TryParse(inputString, out inputInt) && Enum.IsDefined(typeof(InputCommand), inputInt))
            {
                input = (InputCommand)inputInt;   
            }
            else
            {
                Console.WriteLine("Incorrect input. Try again:");
                TryReadInputCommand(out input);
            }


        }


        private static void ReadInputThreshold(out string input, out int threshold)
        {
            Console.WriteLine("Input threshold (0 - 255):");
            input = Console.ReadLine();
            if(input == EXIT_COMMAND)
            {
                threshold = -1;
                return;
            }
            var isInt = int.TryParse(input, out threshold);
            if (isInt)
            {
                var min = byte.MinValue;
                var max = byte.MaxValue;
                if (threshold > max || threshold < min)
                {
                    Console.WriteLine(String.Format("Threshold should be in the range {0} - {1}", min, max));
                    ReadInputThreshold(out input, out threshold);
                }
            }
            else
            {
                Console.Write("Input should be an integer");
                ReadInputThreshold(out input, out threshold);
            }
        }
    }
}
