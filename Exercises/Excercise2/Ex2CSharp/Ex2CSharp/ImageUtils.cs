﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using System.Drawing;

namespace Excercise1CSharp
{
    public class ImageUtils
    {
        public class BinaryImageResult : IDisposable
        {
            public Image<Gray, byte> BinaryImage;
            public PointF CenterOfMass;

            public void Dispose()
            {
                BinaryImage.Dispose();
            }
        }

        public static Image<Bgr,Byte> GenerateHistogram(Mat image, int histWidth = 1044, int histHeight = 720) 
        {
            using (Image<Gray, Byte> img = image.ToImage<Gray, Byte>())
            {
                Array channel; // pre-allocated array holding image data for the color channel or the grayscale image.

                channel = img.Data;

                int[] histogram = new int[Byte.MaxValue + 1]; // histogram array - remember to set to zero initially
                int width = image.Width;
                int height = image.Height;
                int k = Byte.MaxValue;
                while (k-- > 0)
                {
                    histogram[k] = 0; // reset histogram entry
                }
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        var value = img[i, j];
                        histogram[(int)value.Intensity] += 1;
                    }
                }

                // create the output image
                Image<Bgr, Byte> histImage = new Image<Bgr, Byte>(histWidth, histHeight);

                var backgrRect = new Rectangle() { X = 0, Y = 0, Width = histWidth, Height = histHeight };
                histImage.Draw(backgrRect, new Bgr(Color.LightGray), -1); // Thickness < 1 is fill

                int paddingTop = 10, paddingBottom = 10, paddingLeft = 10, paddingRight = 10;

                //Find max value for scaling


                int maxValue = histogram.Max();
                var indexAtMax = histogram.ToList().IndexOf(maxValue);

                LineSegment2DF tempLine = new LineSegment2DF();

                int columnSpacing = (int)Math.Round((histWidth - paddingLeft - paddingRight) / (float)histogram.Length);
                //Draw each column of histogram
                for (int i = 0; i < histogram.Length; i++)
                {
                    int x = (i + 1) * columnSpacing;
                    if (i == 0)
                    {
                        x += paddingLeft;
                    }
                    //Bottom
                    tempLine.P1 = new PointF(x, histHeight - paddingBottom);

                    //Top
                    float topY = (histHeight - paddingBottom - paddingTop) * (1 - (float)histogram[i] / (float)maxValue) + paddingBottom;
                    tempLine.P2 = new PointF(x, topY);

                    histImage.Draw(tempLine, new Bgr(Color.ForestGreen), columnSpacing / 2, Emgu.CV.CvEnum.LineType.AntiAlias);
                }

                Console.WriteLine(String.Format("Frame settings:\n Width: {0}\n Height: {1}\n", histImage.Width, histImage.Height));
                Console.WriteLine(String.Format("Max value: {0} at index {1}", maxValue, indexAtMax));

                return histImage;
            }
        }

        public static BinaryImageResult CreateBinaryImage(Mat image, int threshold)
        {
            var binaryImage = image.Clone().ToImage<Gray, byte>();
            int nonZeroCount = 0;
            double x = 0, y = 0;
            for (int height = 0; height < binaryImage.Height; height++)
            {
                for (int width = 0; width < binaryImage.Width; width++)
                {
                    var value = binaryImage[height, width];
                    if (value.Intensity < threshold)
                    {
                        value.MCvScalar = new MCvScalar(byte.MinValue);
                        x += width;
                        y += height;
                        nonZeroCount++;
                        //value.Intensity = byte.MaxValue;
                    }
                    else
                    {
                        value.MCvScalar = new MCvScalar(byte.MaxValue);
                        //value.Intensity = byte.MinValue;
                    }
                    binaryImage[height, width] = value;
                }
            }
            PointF centerOfMass = new PointF();
            if (nonZeroCount > 0) //Division by zero
            {
                centerOfMass = new PointF() { X = (float)Math.Round(x / nonZeroCount), Y = (float)Math.Round(y / nonZeroCount) };

                //Testing
                //centerOfMass = new PointF(centerOfMass.X - 30, centerOfMass.Y + 30);

                var size = new SizeF() { Height = binaryImage.Height / 15, Width = binaryImage.Height / 15 };
                var cross = new Cross2DF() { Center = centerOfMass, Size = size };

                binaryImage.Draw(cross, new Gray(128), 2);

                var principalAngle = ImageUtils.CalcPrincipalAngle(binaryImage, centerOfMass);
                DrawingUtils.DrawLine(binaryImage, principalAngle, centerOfMass);
            }
            var result = new BinaryImageResult();
            result.BinaryImage = binaryImage;
            result.CenterOfMass = centerOfMass;
            return result;
        }

        public static double CalcPrincipalAngle(Image<Gray,byte> binaryImage, PointF centerOfMass, bool inDeg = false)
        {
            var centralMoment00 = CalcCentralMoment(binaryImage, 0, 0, centerOfMass);
            var redCentralMoment11 = CalcCentralMoment(binaryImage, 1, 1, centerOfMass) / centralMoment00;
            var redCentralMoment20 = CalcCentralMoment(binaryImage, 2, 0, centerOfMass) / centralMoment00;
            var redCentralMoment02 = CalcCentralMoment(binaryImage, 0, 2, centerOfMass) / centralMoment00;

            //var msg = String.Format("Central moments. 00: {0}, red11 {1}, red20 {2}, red02 {3}", centralMoment00, redCentralMoment11, redCentralMoment20, redCentralMoment02);
            //Console.WriteLine(msg);

            var angle = Math.Atan2(2 * redCentralMoment11, (redCentralMoment20 - redCentralMoment02)) / 2; 
            return angle;
        }

        private static double CalcCentralMoment(Image<Gray, byte> binaryImage, int xOrder, int yOrder, PointF centerOfMass)
        {
            double centralMoment = 0;
            for (int y = 0; y < binaryImage.Height; y++)
            {
                for(int x = 0; x < binaryImage.Width; x++)
                {
                    var value = binaryImage[y, x];
                    centralMoment += Math.Pow(x - centerOfMass.X, xOrder) * Math.Pow(y - centerOfMass.Y, yOrder)*Math.Abs(value.Intensity-Byte.MaxValue);
                }
            }
            return centralMoment;
        }

        public static double CalcHuInvariantMoments(Image<Gray, byte> image, PointF centerOfMass)
        {
            return CalcInvariantMoment(image, 2, 0, centerOfMass) + CalcInvariantMoment(image, 0, 2, centerOfMass);
        }

        private static double CalcInvariantMoment(Image<Gray, byte> image, int xOrder, int yOrder, PointF centerOfMass)
        {
            var centralMoment = CalcCentralMoment(image, xOrder, yOrder, centerOfMass);
            var zeroCentralMoment = CalcCentralMoment(image, 0, 0, centerOfMass);

            return centralMoment / Math.Pow(zeroCentralMoment, 1 + (xOrder + yOrder) / 2.0);
        }
    }
}
