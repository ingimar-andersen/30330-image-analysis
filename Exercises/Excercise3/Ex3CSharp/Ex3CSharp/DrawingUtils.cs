﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex3CSharp
{
    public class DrawingUtils
    {
        public static void DrawLine(Image<Gray,byte> image, double angle, PointF point)
        {
            double x1, x2, y1, y2;
            var length = Math.Sqrt(Math.Pow(image.Width, 2) + Math.Pow(image.Height, 2)); //Maximum possible length
            x1 = point.X + length * Math.Cos(angle);
            y1 = point.Y + length * Math.Sin(angle);

            x2 = point.X - length * Math.Cos(angle);
            y2 = point.Y - length * Math.Sin(angle);
            var line = new LineSegment2DF() { P1 = new PointF((float)x1, (float)y1), P2 = new PointF((float)x2, (float)y2) };
            image.Draw(line, new Gray(170), 1);
        }

        internal static void DrawHeaderText(string text, Image<Gray, byte> binaryImage, double scale = 1)
        {
            var x = (int)Math.Round(5 * scale);
            var y = (int)Math.Round(30 * scale);
            binaryImage.Draw(text, new Point(x, y), Emgu.CV.CvEnum.FontFace.HersheySimplex, scale, new Gray(90), 6);
            binaryImage.Draw(text, new Point(x, y), Emgu.CV.CvEnum.FontFace.HersheySimplex, scale, new Gray(215), 2);
        }

        internal static void DrawHeaderText(string text, Image<Rgb, byte> binaryImage, double scale = 1)
        {
            var x = (int)Math.Round(5 * scale);
            var y = (int)Math.Round(30 * scale);
            binaryImage.Draw(text, new Point(x, y), Emgu.CV.CvEnum.FontFace.HersheySimplex, scale, new Rgb(90,90,90), 6);
            binaryImage.Draw(text, new Point(x, y), Emgu.CV.CvEnum.FontFace.HersheySimplex, scale, new Rgb(215,215,215), 2);
        }

    }
}
