﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace Ex3CSharp
{
    class Program
    {
        public const string EXIT_COMMAND = "exit";

        private enum InputCommand
        {
            Exit = 0,
            ImageMoments = 1,
            MomentInvariances = 2,
            Lowpass = 3,
            Highpass = 4,
            HighpassImageStream = 5,
        }

        static void Main(string[] args)
        {
            InputCommand inputCommand;
            TryReadInputCommand(out inputCommand);

            switch (inputCommand)
            {
                case InputCommand.Exit:
                    return;
                case InputCommand.ImageMoments:
                    ImageMoments();
                    break;
                case InputCommand.MomentInvariances:
                    MomentInvariances();
                    break;
                case InputCommand.Lowpass:
                    LowpassFilter();
                    break;
                case InputCommand.Highpass:
                    HighPassFilter();
                    break;
                case InputCommand.HighpassImageStream:
                    HighPassImageStream();
                    break;
                default:
                    return;
            }
        }

        private static void HighPassImageStream()
        {
            ImageViewer viewerNoPreFilter = new ImageViewer(); //create an image viewer
            viewerNoPreFilter.Text = "Highpass without fractile filter";
            ImageViewer viewerWithPreFilter = new ImageViewer(); //create an image viewer
            viewerWithPreFilter.Text = "Highpass with fractile filter";

            ImageViewer viewerOriginal = new ImageViewer(); //create an image viewer
            viewerOriginal.Text = "Original";
            var capture = new VideoCapture(); //create a camera captue
            var cameraFps = GetCameraFps(capture);
            var cameraFrameTimeMs = 1 / cameraFps * 1000;
            var frameStatArraySize = 60;
            var frameTimesMs = new long[frameStatArraySize];
            var processTimesMs = new double[frameStatArraySize];
            var frameStatIndex = 0;
            var sw = new Stopwatch();
            sw.Start();
            Application.Idle += new EventHandler(delegate (object sender, EventArgs e)
            {  //run this until application closed (close button click on image viewer)
                var source = capture.QueryFrame();
                var processingStartedMs = sw.ElapsedMilliseconds;
                viewerOriginal.Image = source;
                //var filterResult = Filters.LowpassCV(source.ToImage<Gray, byte>().Mat, 1);
                //filterResult = Filters.HighpassCV(filterResult, Filters.HighPassFilters.LaplaceGaussian);


                var testStart = sw.ElapsedMilliseconds;
                //Gray
                var filterResultNoPreFilter = Filters.FilterCV(source.ToImage<Gray, byte>().Mat, FilterDefinitions.FilterType.LaplaceGaussianHP);
                //var filterResultWithPreFilter = Filters.FilterCV(source.ToImage<Gray, byte>().SmoothMedian(5).Mat, FilterDefinitions.FilterType.LaplaceGaussianHP);
                //Color
                //var filterResult = Filters.HighpassCV(source, Filters.HighPassFilters.LaplaceGaussian);

                //Fractile filter
                //var filterResult = Filters.FractileFilter(source.ToImage<Gray, byte>(), 2, 0.7);
                //var filterResult = source.ToImage<Rgb, byte>().SmoothMedian(5);
                //var image = filterResult;

                var testTime = sw.ElapsedMilliseconds - testStart;

                //Non-Inverted
                //var image = filterResult.ToImage<Rgb, byte>().Not();
                var imageNoPreFilter = filterResultNoPreFilter.ToImage<Gray, byte>().Not();
                //var imageWithPreFilter = filterResultWithPreFilter.ToImage<Gray, byte>().Not();
                //Inverted
                //var image = filterResult.ToImage<Rgb, byte>();


                var processTime = sw.ElapsedMilliseconds - processingStartedMs;
                var fps = 1000.0 * frameStatArraySize / frameTimesMs.Sum();
                var processTimeAvg = processTimesMs.Average();
                DrawingUtils.DrawHeaderText(String.Format("{0:0.0} fps, usage: {1:0.0} % of {2:0.0} ms", fps, processTimeAvg, cameraFrameTimeMs), imageNoPreFilter, 0.7);
                viewerNoPreFilter.Image = imageNoPreFilter.Mat;
                //viewerWithPreFilter.Image = imageWithPreFilter.Mat;

                processTimesMs[frameStatIndex] = (double)processTime / cameraFrameTimeMs * 100.0;
                frameTimesMs[frameStatIndex] = sw.ElapsedMilliseconds;
                sw.Restart();
                if (frameStatIndex >= frameStatArraySize - 1) {
                    frameStatIndex = 0;
                }
                else
                {
                    frameStatIndex++;
                }
            });
            viewerOriginal.Show();
            //viewerWithPreFilter.Show();
            viewerNoPreFilter.ShowDialog(); //show the image viewer
        }

        private static double GetCameraFps(VideoCapture capture)
        {
            var cameraFps = capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);
            
            //The Fps property is not accessible on webcams.
            //Manual calculation fallback
            if (cameraFps <= 0) 
            {
                var numberOfFrames = 60;
                var sw = new Stopwatch();
                capture.QueryFrame(); // First time takes longer due to some internal initialization. Run before timing to minimize effect.
                sw.Start();
                for (int i = 0; i < numberOfFrames; i++)
                {
                    capture.QueryFrame();
                }
                sw.Stop();

                double time = sw.ElapsedMilliseconds / 1000.0; // Elapsed time in seconds

                cameraFps = numberOfFrames / time;
            }

            return cameraFps;
        }

        private static void HighPassFilter()
        {
            var image = CvInvoke.Imread("C:\\Users\\grena\\OneDrive\\DTU\\2017 - Haust\\30330 Image Analysis with Microcomputer\\CampusNet\\ariane5_1b.jpg", Emgu.CV.CvEnum.ImreadModes.AnyColor);
            var windowName1 = "Original";
            CvInvoke.Imshow(windowName1, image);

            var sw = new Stopwatch();
            sw.Start();
            var filterResult1 = Filters.FilterCV(image, FilterDefinitions.FilterType.HomeMadeHP);
            sw.Stop();
            var myFilterMs = sw.ElapsedMilliseconds;
            CvInvoke.Imshow(String.Format("HP CV homemade n={0}", 2), filterResult1);

            sw.Restart();
            var filterResult2 = Filters.FilterCV(image, FilterDefinitions.FilterType.HomeMadeHP2);
            sw.Stop();
            var myFilterMs2 = sw.ElapsedMilliseconds;
            CvInvoke.Imshow(String.Format("HP CV homemade 2 n={0}", 2), filterResult2);

            sw.Restart();
            var filterResult3 = Filters.FilterCV(image, FilterDefinitions.FilterType.LaplaceGaussianHP);
            sw.Stop();
            var myFilterMs3 = sw.ElapsedMilliseconds;
            CvInvoke.Imshow(String.Format("HP CV LG n={0}", 4), filterResult3);

            Console.WriteLine(String.Format("My HP filter execution time: {0} [ms]", myFilterMs));
            Console.WriteLine(String.Format("My HP filter v2 execution time: {0} [ms]", myFilterMs2));
            Console.WriteLine(String.Format("Laplace-gaussian HP filter execution time: {0} [ms]", myFilterMs3));

            CvInvoke.WaitKey();

            image.Dispose();

            filterResult1.Dispose();
            filterResult2.Dispose();
            filterResult3.Dispose();
        }

        private static void LowpassFilter()
        {
            //var image = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise3\\Ex3CSharp\\Ex3CSharp\\PEN.pgm", Emgu.CV.CvEnum.ImreadModes.AnyColor);
            var image = CvInvoke.Imread("C:\\Users\\grena\\OneDrive\\DTU\\2017 - Haust\\30330 Image Analysis with Microcomputer\\CampusNet\\ariane5_1b.jpg", Emgu.CV.CvEnum.ImreadModes.AnyColor);
            var windowName1 = "Original";
            CvInvoke.Imshow(windowName1, image);


            //Homemade LP
            var grayImage = image.ToImage<Gray, byte>();
            uint n = 3;
            var sw = new Stopwatch();
            sw.Start();
            var filterResult = Filters.LowpassAverage(grayImage, n);
            sw.Stop();
            var myFilterMs = sw.ElapsedMilliseconds;

            CvInvoke.Imshow(String.Format("LP avg n={0}", n), filterResult);

            //OpenCV LP
            sw.Restart();
            var cvFilterResult = Filters.LowPassAvgCV(grayImage.Mat, n);
            sw.Stop();
            var cvFilterMs = sw.ElapsedMilliseconds;

            CvInvoke.Imshow(String.Format("CV LP n={0}", n), cvFilterResult);


            //Homemad Fractile LP
            sw.Restart();
            var fracFilterResult = Filters.FractileFilter(grayImage, n, 0.5);
            sw.Stop();
            var fracFilterMs = sw.ElapsedMilliseconds;

            CvInvoke.Imshow(String.Format("LP fractile avg n={0}", n), fracFilterResult);

            //OpenCV Fractile (median) LP
            sw.Restart();
            var cvFracFilterResult = grayImage.SmoothMedian((int)(2*n + 1));
            sw.Stop();
            var cvFracFilterMs = sw.ElapsedMilliseconds;

            CvInvoke.Imshow(String.Format("CV fractile avg n={0}", n), cvFracFilterResult);

            //Debug
            Console.WriteLine(String.Format("My lowpass filter execution time: {0} [ms]", myFilterMs));
            Console.WriteLine(String.Format("OpenCV lowpass filter execution time: {0} [ms]", cvFilterMs));
            Console.WriteLine(String.Format("My fractile filter execution time: {0} [ms]", fracFilterMs));
            Console.WriteLine(String.Format("OpenCV fractile (smoothMedian) filter execution time: {0} [ms]", cvFracFilterMs));

            CvInvoke.WaitKey();

            grayImage.Dispose();
            image.Dispose();
        }

        private static void MomentInvariances()
        {
            var image1 = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise2\\Ex2CSharp\\Ex2CSharp\\WalletStraightScaledDown.png", Emgu.CV.CvEnum.ImreadModes.AnyColor);
            //var image1 = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise2\\Ex2CSharp\\Ex2CSharp\\PEN.pgm", Emgu.CV.CvEnum.ImreadModes.AnyColor);
            var image2 = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise2\\Ex2CSharp\\Ex2CSharp\\WalletRotated.png", Emgu.CV.CvEnum.ImreadModes.AnyColor);
            string input = "";
            int threshold = 0; // 100 is the magic number
            ReadInputThreshold(out input, out threshold);
            while (input.ToLower() != EXIT_COMMAND)
            {
                //Thread that handles the image processing. The main thread is reserved for user input.
                new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;

                    //Image 1
                    var binaryImage1 = ImageUtils.CreateBinaryImage(image1, threshold);
                    var huInvariantMoment1 = ImageUtils.CalcHuInvariantMoments(binaryImage1.BinaryImage, binaryImage1.CenterOfMass);

                    DrawingUtils.DrawHeaderText(String.Format("HU: {0:E2}", huInvariantMoment1), binaryImage1.BinaryImage);

                    var windowName1 = String.Format("BI 1 {0}", threshold);
                    CvInvoke.Imshow(windowName1, binaryImage1.BinaryImage);


                    //Image 2
                    var binaryImage2 = ImageUtils.CreateBinaryImage(image2, threshold);
                    var huInvariantMoment2 = ImageUtils.CalcHuInvariantMoments(binaryImage2.BinaryImage, binaryImage2.CenterOfMass);

                    var percentageMatch = Math.Min(huInvariantMoment1, huInvariantMoment2) / Math.Max(huInvariantMoment1, huInvariantMoment2);
                    DrawingUtils.DrawHeaderText(String.Format("HU: {0:E2}, Match: {1:P}", huInvariantMoment2, percentageMatch), binaryImage2.BinaryImage);

                    var windowName2 = String.Format("BI 2 {0}", threshold);
                    CvInvoke.Imshow(windowName2, binaryImage2.BinaryImage);

                    CvInvoke.WaitKey(); //Just to keep the thread alive. The thread is killed once all windows are closed.

                    binaryImage1.Dispose();
                    binaryImage2.Dispose();

                }).Start();

                ReadInputThreshold(out input, out threshold);
            }

        }

        private static void ImageMoments()
        {
            try
            {
                string input = "";
                int threshold = 0;
                while (true)
                {
                    ReadInputThreshold(out input, out threshold);
                    if(input == EXIT_COMMAND)
                    {
                        return;
                    }
                    GenerateImageMomentWindows(threshold);
                }
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }

        private static void GenerateImageMomentWindows(int threshold)
        {
            new Thread(() => {
                //image = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise2\\Ex2CSharp\\Ex2CSharp\\PEN.pgm", Emgu.CV.CvEnum.ImreadModes.AnyColor);
                var image = CvInvoke.Imread("C:\\Repositories\\30330-Image-Analysis\\Exercises\\Excercise2\\Ex2CSharp\\Ex2CSharp\\WalletStraight.png", Emgu.CV.CvEnum.ImreadModes.AnyColor);
                //image = CvInvoke.Imread("C:\\Users\\grena\\OneDrive\\DTU\\2017 - Haust\\30330 Image Analysis with Microcomputer\\TestImages\\WalletRotated.png", Emgu.CV.CvEnum.ImreadModes.AnyColor);
                CvInvoke.Imshow("Original", image);

                using (var binaryImage = ImageUtils.CreateBinaryImage(image, threshold))
                {
                    CvInvoke.Imshow(String.Format("BI {0}", threshold), binaryImage.BinaryImage);

                    using (var hist = ImageUtils.GenerateHistogram(image))
                    {
                        CvInvoke.Imshow("Histogram", hist);
                        CvInvoke.WaitKey(); //Keep the thread alive
                    }
                }

                    
            }).Start();
        }

        private static void TryReadInputCommand(out InputCommand input)
        {
            Console.WriteLine("You have the following options:\n[1] for Part 3: Image moments\n[2] for Part 4: Moment invariances\n[3] Lowpass filters\n[4] Highpass filters\n[5] Highpass filter stream");
            string inputString = Console.ReadLine();
            inputString = inputString.Trim();
            int inputInt;
            if (int.TryParse(inputString, out inputInt) && Enum.IsDefined(typeof(InputCommand), inputInt))
            {
                input = (InputCommand)inputInt;   
            }
            else
            {
                Console.WriteLine("Incorrect input. Try again:");
                TryReadInputCommand(out input);
            }


        }


        private static void ReadInputThreshold(out string input, out int threshold)
        {
            Console.WriteLine("Input threshold (0 - 255):");
            input = Console.ReadLine();
            if(input == EXIT_COMMAND)
            {
                threshold = -1;
                return;
            }
            var isInt = int.TryParse(input, out threshold);
            if (isInt)
            {
                var min = byte.MinValue;
                var max = byte.MaxValue;
                if (threshold > max || threshold < min)
                {
                    Console.WriteLine(String.Format("Threshold should be in the range {0} - {1}", min, max));
                    ReadInputThreshold(out input, out threshold);
                }
            }
            else
            {
                Console.Write("Input should be an integer");
                ReadInputThreshold(out input, out threshold);
            }
        }
    }
}
