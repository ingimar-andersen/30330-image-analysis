﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Europa
{
    public class FrameResult
    {
        public PointF Translation;
        public double Rotation;
        public PointF TotalTranslation;
        public double TotalRotation;
        public float Altitude;
        public double CalculationTimeMs;
        public bool FeatureUpdate;
        public int DroppedFrames;

        internal FrameResult Clone()
        {
            return new FrameResult
            {
                Translation = this.Translation,
                Rotation = this.Rotation,
                TotalTranslation = this.TotalTranslation,
                TotalRotation = this.TotalRotation,
                Altitude = this.Altitude,
                CalculationTimeMs = this.CalculationTimeMs,
                FeatureUpdate = this.FeatureUpdate,
                DroppedFrames = this.DroppedFrames,
            };
        }
    }
}
