﻿using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Europa
{
    public class PathResult
    {
        public List<LineSegment2DF> Segments;
        public PointF Translation;
        public double Rotation;
    }
}
