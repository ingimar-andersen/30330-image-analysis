﻿using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Europa
{
    public enum TrackerType
    {
        KLT = 1,
        Farneback = 2,
    }

    public enum DetectorType
    {
        GFTT = 1,
        ORB = 2,
    }

    public class KltHelper
    {
        //Program parameters
        private static int FeaturesPerSegment = 20;
        private static double MinDistance = 15;
        private static double FeaturesToFindScaling = 1.2;
        private static int FrameDivision = 10;
        private const float SimulatedMHz = 200;
        private static float ActualMHz;
        private const int SimulatedFps = 3;
        private static float MaxActualCalcTimeMs;
        public static int UpdateFeaturesDivision = 15;

        private static int Border = 20; //Eliminate tracking errors where tracking point follows a line because point of interest is out of the image
        private static DetectorType _detectorType;
        //ALSO constructor arguements
        //Number of segments
        //Disregard corners

        private static TrackerType _trackerType; //Only one implemented

        //OpenCV parameters
        private static double QualityLevel = 0.01;
        private static Size winSize = new Size(11, 11);
        private static MCvTermCriteria criteria = new MCvTermCriteria(20, 0.01);
        private static int _pyramidLevel;

        //UI
        private static ImageViewer viewer;
        private static ImageViewer viewer2;

        private static List<PointF> _keyPoints;
        private static Mat _startFrame;
        private static Image<Gray, byte> _previousGrayFrame;
        private static bool IsFirstFrame = true;
        private static int _frameCount = 0;
        private static List<FrameResult> frameResults = new List<FrameResult>();

        private static Rectangle _boundingBox;

        private static List<KltImageSegment> segments;

        //Input
        private static bool isVideo;
        private static string _fileExtension;
        private string[] VideoExtensions = new string[] { ".avi", ".mp4" };
        private static VideoCapture capture;
        private static string _fileNameAppendix;
        private static bool DropFrames;

        public static List<SourceFile> SourceFiles { get; set; }

        private static string _captureSourcePath;
        private static string _captureDataPath
        {
            get
            {
                var dir = Path.GetDirectoryName(_captureSourcePath);
                var filename = Path.GetFileNameWithoutExtension(_captureSourcePath) + "_" + _fileNameAppendix + ".txt";
                return Path.Combine(dir, filename);
            }
        }

        //Image sequences
        private static string _capturePathImagePath
        {
            get
            {
                var dir = Path.GetDirectoryName(_captureSourcePath);
                var filename = Path.GetFileNameWithoutExtension(_captureSourcePath) + "_" + _fileNameAppendix + ".png";
                return Path.Combine(dir, filename);
            }
        }

        private static float _altitudeProportion;
        private static float terminalAltitudeProprotion;
        private static float _deltaAltitude;

        private static int minNoFeaturesInSegmentCenter;
        private static int minNoFeaturesInSegmentEdge;

        
        //Center point
        private static Point CenterPoint;
        private static SizeF CenterPointSize;
        private static PointF _centerPointF;
        private static PointF CenterPointF
        {
            set
            {
                CenterPoint = Point.Round(value);
                _centerPointF = value;
                CenterPointSize = new SizeF(CenterPointF);
            }
            get
            {
                return _centerPointF;
            }
        }

        //For data output and measurements
        private static Stopwatch _sw = new Stopwatch();
        private static Pose endPose;
        private static int droppedFrames = 0;

        public KltHelper(TrackerType trackerType, DetectorType detectorType, int pyramidLevel, int noOfSegments = 25, bool disregardCorners = false, string fileNameAppendix = "", bool dropFrames = false)
        {
            _trackerType = trackerType;
            _detectorType = detectorType;
            _pyramidLevel = pyramidLevel;
            _fileNameAppendix = dropFrames ? fileNameAppendix + $"_{SimulatedFps:0}fps_dropframes" : fileNameAppendix;
            DropFrames = dropFrames;
            var sourceFolderPath = "C:\\Users\\grena\\OneDrive\\DTU\\2017 - Haust\\30330 Image Analysis with Microcomputer\\TestVideos\\Translation\\Constant altitude";
            SourceFiles = Directory.GetFiles(sourceFolderPath)
                .Where(x => VideoExtensions.Contains(Path.GetExtension(x)))
                .Select(x => new SourceFile(x)).ToList();
            _captureSourcePath = GetNextSourcePath();
            //_captureSourcePath = "C:\\Users\\grena\\OneDrive\\DTU\\2017 - Haust\\30330 Image Analysis with Microcomputer\\TestVideos\\Europa rotation\\Europa rotation_.png"; //Format for 
            _fileExtension = Path.GetExtension(_captureSourcePath);

            ActualMHz = HardwareUtils.GetCpuClockSpeedMHz();
            MaxActualCalcTimeMs = (1 / (float)SimulatedFps) / (HardwareUtils.GetCpuClockSpeedMHz() / SimulatedMHz) * 1000;

            isVideo = VideoExtensions.Contains(_fileExtension);
            _altitudeProportion = 1.0f;
            terminalAltitudeProprotion = 1.0f; //_captureSourcePath.ToLower().Contains("constant") ? 1.0f : 0.5f; //Yes, I am a mad man.
            endPose = new Pose() { X = 225, Y = 225, AngleDeg = 0, AltitudeProportion =  terminalAltitudeProprotion};
            if (isVideo)
            {
                capture = new VideoCapture(_captureSourcePath);
                _deltaAltitude = (_altitudeProportion - terminalAltitudeProprotion) / (float)capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount);
            }
            else
            {
                var files = Directory.GetFiles(Path.GetDirectoryName(_captureSourcePath));
                Array.Sort(files);
                var last = files[files.Length - 1];
                var lastName = Path.GetFileNameWithoutExtension(last);
                var frameCountString = lastName.Substring(lastName.Length - 5, 5);
                var frameCount = int.Parse(frameCountString) + 1;
                _deltaAltitude = (_altitudeProportion - terminalAltitudeProprotion) / frameCount;
            }

            viewer = new ImageViewer(); //create an image viewer
            viewer.Text = "KLT";

            viewer2 = new ImageViewer();
            viewer2.Text = "Original gray";

            //Ensure the viewer resizes with the source video
            viewer.ImageBox.SizeMode = PictureBoxSizeMode.AutoSize;
            viewer.AutoSize = true;

            viewer2.ImageBox.SizeMode = PictureBoxSizeMode.AutoSize;
            viewer2.AutoSize = true;

            var framePath = GetFramePath();
            _startFrame = isVideo ? capture.QueryFrame() : CvInvoke.Imread(framePath);
            //var firstFrame = CvInvoke.Imread(framePath);

            _previousGrayFrame = new Image<Gray, byte>(_startFrame.Bitmap);

            CenterPointF =  new PointF(_startFrame.Width / 2, _startFrame.Height / 2);

            _boundingBox = new Rectangle(Border, Border, _startFrame.Width - 2*Border, _startFrame.Height - 2*Border);

            var nSegment = (int)Math.Round(Math.Sqrt(noOfSegments));

            var segmentWidth = (int)Math.Round(1.0 * _boundingBox.Width / nSegment);
            var segmentHeight = (int)Math.Round(1.0 * _boundingBox.Height / nSegment);

            segments = new List<KltImageSegment>();

            for (int x = 0; x < nSegment; x++ ){
                for(int y = 0; y < nSegment; y++)
                {
                    if (disregardCorners && IsCorner(x, y, 0, nSegment - 1)) continue;
                    var segment = new Rectangle(Border + x * segmentWidth, Border + y * segmentHeight, segmentWidth, segmentHeight);
                    var isEdge = x == 0 || x == nSegment - 1 || y == 0 || y == nSegment - 1;
                    segments.Add(new KltImageSegment() { BoundingBox = segment, IsEdge = isEdge});
                }
            }
            CvInvoke.WaitKey();
        }

        private static string GetNextSourcePath()
        {
            var first = SourceFiles.FirstOrDefault(x => !x.HasBeenProccessed);
            if (first == null) return null; 
            first.HasBeenProccessed = true;
            return first.Path;
        }

        private bool IsCorner(int x, int y, int min, int max)
        {
            if (x == min && y == min) return true;
            if (x == max && y == max) return true;
            if (x == min && y == max) return true;
            if (x == max && y == min) return true;
            return false;
        }

        public DialogResult Start()
        {
            Application.Idle += ApplicationIdle_Handler;
            //viewer2.Show();
            return viewer.ShowDialog(); //show the image viewer
            //return new DialogResult();
        }

        private static bool ForceUpdateFeatures = false;

        private static void ApplicationIdle_Handler(object sender, EventArgs e)
        {
            //run this until application closed (close button click on image viewer)
            _frameCount++;
            Mat queryFrame = null;
            if (isVideo)
            {
                queryFrame = capture.QueryFrame();
            }
            else
            {
                queryFrame = Emgu.CV.CvInvoke.Imread(GetFramePath());
            }
          
            if(_frameCount % FrameDivision == 0)
            {
                _altitudeProportion -= _deltaAltitude * FrameDivision;
                if (DropFrames && droppedFrames > 0)
                {
                    droppedFrames--;
                    var lastCopy = frameResults.Last().Clone();
                    lastCopy.CalculationTimeMs = 0;
                    lastCopy.DroppedFrames = 0;
                    lastCopy.FeatureUpdate = false;
                    lastCopy.Altitude = _altitudeProportion;
                    frameResults.Add(lastCopy);
                    viewer.Focus(); // So that the loop continues
                    if (_frameCount / FrameDivision % UpdateFeaturesDivision == 0)
                    {
                        ForceUpdateFeatures = true; //In case a feature update
                    }
                }
                else
                {
                    droppedFrames = ProcessFrame(queryFrame, ForceUpdateFeatures);
                    ForceUpdateFeatures = false;
                }
            }
            else
            {
                viewer.Focus(); // So that the loop continues
            }
            queryFrame?.Dispose();
        }

        private static string GetFramePath()
        {
            var dir = Path.GetDirectoryName(_captureSourcePath);
            var file = Path.GetFileNameWithoutExtension(_captureSourcePath) + _frameCount.ToString("00000") + _fileExtension;
            return Path.Combine(dir, file);
        }

        private static int ProcessFrame(Mat queryFrame, bool forceUpdateFeatures = false)
        {
            //Save data and go to next video sequence
            if (queryFrame == null) //We have reaced the end
            {
                SaveData();
                Reset();
                return 0;
            }

            _sw.Restart();
            var frameResult = new FrameResult();
            var grayFrame = new Image<Gray, byte>(queryFrame.Bitmap);

            //Update features
            if (_keyPoints == null || _frameCount/FrameDivision % UpdateFeaturesDivision == 0 || forceUpdateFeatures)
            {
                frameResult.FeatureUpdate = true; //For analysis purposes
                if (_keyPoints == null) _keyPoints = new List<PointF>();
                var noFeaturesToFind = (int)Math.Round(FeaturesPerSegment * FeaturesToFindScaling); //Find 50% more features than needed in case too many overlap the minimum distance

                using (Feature2D detector = GetDetector(noFeaturesToFind, _detectorType))
                {
                    UpdateFeatures(detector);
                }
            }
            else
            {
                frameResult.FeatureUpdate = false; //For analysis purposes
            }

            PointF[] currentFeatures = null;
            byte[] status = null;
            float[] trackError = null;

            if (_trackerType == TrackerType.KLT)
            {
                CvInvoke.CalcOpticalFlowPyrLK(_previousGrayFrame, grayFrame, _keyPoints.ToArray(), winSize, _pyramidLevel, criteria, out currentFeatures, out status, out trackError, Emgu.CV.CvEnum.LKFlowFlag.Default);
            }
            else if (_trackerType == TrackerType.Farneback)
            {
                //CvInvoke.CalcOpticalFlowFarneback()
            }
            else
            {
                throw new Exception($"Unsupported tracker type {_trackerType.ToString()}");
            }

            if (status.Any(x => x == 0x0))
            {
                Console.WriteLine($"Optical flow not found for {status.Count(x => x == 0x0)} points");
            }

            var deltaAngle = CalcDeltaAngle(currentFeatures, trackError, status, disregardWorstProportion: 0.1);

            frameResult.Rotation = deltaAngle;

            var deltaPosition = CalcDeltaPosition(currentFeatures, trackError, status, deltaAngle, disregardWorstProportion: 0.1);
            frameResult.Translation = deltaPosition;
            frameResult.Altitude = _altitudeProportion;

            //Lost features
            var featuresWithinFrame = currentFeatures.Where(f => _boundingBox.Contains(Point.Round(f)));
            if (_keyPoints.Count() > featuresWithinFrame.Count())
            {
                var lostFeatures = _keyPoints.Count() - featuresWithinFrame.Count();
                Console.WriteLine($"{lostFeatures} features lost");
            }

            //Update keypoints
            _keyPoints = new List<PointF>(featuresWithinFrame);

            var path = ReconstructPath();
            frameResult.TotalRotation = path.Rotation;
            frameResult.TotalTranslation = path.Translation;

            var timeMs = _sw.ElapsedMilliseconds;
            var droppedFrames = (int)Math.Ceiling(timeMs / MaxActualCalcTimeMs) - 1;
            frameResult.DroppedFrames = droppedFrames;
            frameResult.CalculationTimeMs = timeMs;

            frameResults.Add(frameResult);

            //Draw stuff
            var resultImage = DrawingUtils.DrawPoints(queryFrame.Bitmap, _keyPoints, Color.FromArgb(0, 255, 0));
            DrawingUtils.DrawHeaderText($"{timeMs} [ms]", resultImage, 1, bottom: true);
            DrawingUtils.DrawHeaderText($"Trans: ({path.Translation.X.ToString("0.0")},{path.Translation.Y.ToString("0.0")})  Rot: {GeometricUtils.Rad2Deg(path.Rotation).ToString("0.0")}deg", resultImage);
            DrawingUtils.DrawLines(resultImage, path.Segments, Color.Red);
            DrawSegmentBoxes(resultImage);

            //Fit viewer to image size
            if (viewer.ImageBox.Size != grayFrame.Size)
            {
                viewer.ImageBox.Size = grayFrame.Size;
            }
            viewer.Image = resultImage;

            resultImage.Dispose();
            if (IsFirstFrame) //Make sure everything is initialized during first run and then run again to get a more realistic time measurement
            {
                IsFirstFrame = false;
                _frameCount = 0;
                _altitudeProportion = 1f;
                _keyPoints = null;
                frameResults = new List<FrameResult>();
                return ProcessFrame(queryFrame);
            }
            _previousGrayFrame = grayFrame;
            
            return droppedFrames;
        }

        private static PointF CalcDeltaPosition(PointF[] features, float[] trackError, byte[] status, double deltaAngle, double disregardWorstProportion)
        {
            if (disregardWorstProportion < 0.0 || disregardWorstProportion >= 1.0) throw new Exception("Disregard proportion should be between 0.0 and 1.0");
            var deltaX = new List<float>();
            var deltaY = new List<float>();
            foreach (var segment in segments.Where(x => !x.IsEdge))
            {
                var kpCount = 0;

                //Features with lowest tracking error first
                var segmentFeatures = _keyPoints.Select((Value, Index) => new { Value, Index, Error = trackError[Index] }).Where(kp => segment.BoundingBox.Contains(Point.Round(kp.Value)));
                var errorValues = segmentFeatures.Select(x => x.Error).ToArray();
                Array.Sort(errorValues);
                int medianIndex = (int)Math.Round(errorValues.Length / 2.0);
                var errorMedian = errorValues[medianIndex];
                var segmentFeaturesOrdered = segmentFeatures.Select(x => new { x.Value, Error = Math.Abs(x.Error - errorMedian), Index = x.Index }).OrderBy(x => x.Error);
                foreach (var sf in segmentFeaturesOrdered)
                {
                    if (status[sf.Index] == 0x0) continue; //Optical flow not found
                    var prev = sf.Value;
                    kpCount++;
                    
                    //var prevPrediction = PointF.Add(GeometricUtils.RotatePoint(PointF.Subtract(prev, CenterPointSize), deltaAngle), CenterPointSize);
                    var curr = features[sf.Index];
                    //deltaX2.Add(curr.X - prevPrediction.X);
                    //deltaY2.Add(curr.Y - prevPrediction.Y);

                    deltaX.Add(curr.X - prev.X);
                    deltaY.Add(curr.Y - prev.Y);

                    if (kpCount >= Math.Round(minNoFeaturesInSegmentCenter * (1 - disregardWorstProportion))) //Use same amount of keypoints per segment
                    {
                        break;
                    }
                }
            }
            return new PointF(deltaX.Any() ? deltaX.Average() : 0, deltaY.Any() ? deltaY.Average() : 0);
        }

        private static void DrawSegmentBoxes(Image<Rgb, float> resultImage)
        {
            foreach (var segment in segments)
            {
                resultImage.Draw(segment.BoundingBox, new Rgb(0, 0, 255));
            }
        }

        private static PathResult ReconstructPath()
        {
            var pathSegmentsForFrame = new List<LineSegment2DF>();
            var deltaAngleDeg = frameResults.Select(x => x.Rotation).Sum() / (2 * Math.PI) * 360;
            var prevPoint = CenterPointF;
            double deltaAngleSum = 0;
            for (int i = frameResults.Count() - 1; i >= 0; i--)
            {
                var d = frameResults[i];
                deltaAngleSum += d.Rotation;
                float reverseAltitudeScaling = (float)(d.Altitude / _altitudeProportion);

                float x = d.Translation.X * reverseAltitudeScaling;
                float y = d.Translation.Y * reverseAltitudeScaling;
                var rotated = GeometricUtils.RotatePoint(new PointF(x, y), deltaAngleSum);
                var currPoint = PointF.Add(prevPoint, new SizeF(rotated));
                pathSegmentsForFrame.Add(new LineSegment2DF(prevPoint,currPoint));

                prevPoint = currPoint;
            }
            // prevPoint is the position of the start position relative to the current frame
            var rotatedSum = GeometricUtils.RotatePoint(new PointF((prevPoint.X - CenterPointF.X) * _altitudeProportion, (prevPoint.Y - CenterPointF.Y) * _altitudeProportion), -deltaAngleSum);
           
            return new PathResult()
            {
                Segments = pathSegmentsForFrame,
                Translation = rotatedSum,
                Rotation = deltaAngleSum,
            };
        }

        private static double CalcDeltaAngle(PointF[] features, float[] trackError, byte[] status, double disregardWorstProportion)
        {
            if (disregardWorstProportion < 0.0 || disregardWorstProportion >= 1.0) throw new Exception("Disregard proportion should be between 0.0 and 1.0");
            var deltaAngles = new List<double>();
            foreach (var segment in segments.Where(x => x.IsEdge))
            {
                var kpCount = 0;

                //Features with lowest tracking error first
                var segmentFeatures = _keyPoints.Select((Value, Index) => new { Value, Index, Error = trackError[Index]}).Where(kp => segment.BoundingBox.Contains(Point.Round(kp.Value)));
                var errorValues = segmentFeatures.Select(x => x.Error).ToArray();
                Array.Sort(errorValues);
                int medianIndex = (int)Math.Round(errorValues.Length / 2.0);
                var errorMedian = errorValues[medianIndex];
                var segmentFeaturesOrdered = segmentFeatures.Select(x => new { x.Value, Error = Math.Abs(x.Error - errorMedian), Index = x.Index }).OrderBy(x => x.Error);
                foreach (var sf in segmentFeaturesOrdered)
                {
                    if (status[sf.Index] == 0x0) continue; //Optical flow not found
                    var prev = sf.Value;
                    kpCount++;
                    var curr = features[sf.Index];
                    deltaAngles.Add(GeometricUtils.CalcAngle(CenterPointF, curr, prev));
                    if (kpCount >= Math.Round(minNoFeaturesInSegmentCenter * (1 - disregardWorstProportion))) //Use same amount of keypoints per segment
                    {
                        break;
                    }
                }
            }
            return deltaAngles.Any() ? deltaAngles.Average() : 0;
        }

        private static void UpdateFeatures(Feature2D detector)
        {
            minNoFeaturesInSegmentCenter = int.MaxValue;
            minNoFeaturesInSegmentEdge = int.MaxValue;
            foreach (var segment in segments)
            {
                var segmentKeyPoints = _keyPoints.Where(f => segment.BoundingBox.Contains(Point.Round(f))).ToList();
                var segmentKeyPointCount = segmentKeyPoints.Count();
                if (segmentKeyPointCount >= FeaturesPerSegment) continue;

                //New features inside the segment in image coordinates
                var newFeatures = detector.Detect(_previousGrayFrame.GetSubRect(segment.BoundingBox))
                    .Select(p => new PointF(p.Point.X + segment.BoundingBox.X, p.Point.Y + segment.BoundingBox.Y)); //Translating the feature points into baseframe coordinates

                foreach (var kp in newFeatures)
                {
                    var tooClose = MinDistance <= 0 ? false :_keyPoints.Any(x => GeometricUtils.Distance(x, kp) < MinDistance);
                    if (!tooClose)
                    {
                        _keyPoints.Add(kp);
                        segmentKeyPointCount++;
                        if (segmentKeyPointCount >= FeaturesPerSegment) break;
                    }
                }

                //Update minimum count of features per segment
                if (segment.IsEdge)
                {
                    minNoFeaturesInSegmentEdge = Math.Min(minNoFeaturesInSegmentEdge, segmentKeyPointCount);
                }
                else
                {
                    minNoFeaturesInSegmentCenter = Math.Min(minNoFeaturesInSegmentCenter, segmentKeyPointCount);
                }
            }
        }

        private static Feature2D GetDetector(int noFeaturesToFind, DetectorType detectorType)
        {
            Feature2D detector = null;
            if (detectorType == DetectorType.GFTT)
            {
                detector = new GFTTDetector(
                    noFeaturesToFind,
                    QualityLevel,
                    MinDistance,
                    blockSize: 3,
                    useHarrisDetector: true);
            }
            else if (detectorType == DetectorType.ORB)
            {
                detector = new Emgu.CV.Features2D.ORBDetector(
                    noFeaturesToFind,
                    scaleFactor: 1.2f,
                    nLevels: 8,
                    edgeThreshold: 31,
                    firstLevel: 0,
                    WTK_A: 2,
                    scoreType: ORBDetector.ScoreType.Harris,
                    patchSize: 31,
                    fastThreshold: 20);
            }
            else
            {
                throw new Exception($"Unsupported detector type {_detectorType.ToString()}");
            }
            return detector;
        }

        private static void SaveData()
        {
            var dataPath = _captureDataPath;
            var pathImagePath = _capturePathImagePath;

            var tempDataPath = Path.GetTempFileName();
            var tempPathImagePath = Path.GetTempFileName();
            tempPathImagePath = Path.Combine(Path.GetDirectoryName(tempPathImagePath), Path.GetFileNameWithoutExtension(tempPathImagePath)) + ".png"; //Must have a image format for saving in OpenCV

            var pathSegments = new List<LineSegment2D>();
            var prevPoint = new PointF(0, 0);
            using (var sw = File.AppendText(tempDataPath))
            {
                var fps = capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps) / FrameDivision;
                var deltaT = 1 / fps;
                var time = 0.0;
                sw.WriteLine($"t [s]\tx [pixels]\ty [pixels]\tangle [deg]\taltitude [%]\texe time [ms]\texe time @{SimulatedMHz}MHz [ms]\tDropped frames\tFeatureUpdate [bool]");
                for (int i = 0; i < frameResults.Count(); i++)
                {
                    time += deltaT;
                    var d = frameResults[i];
                    sw.WriteLine($"{time}\t{d.TotalTranslation.X}\t{d.TotalTranslation.Y}\t{GeometricUtils.Rad2Deg(d.TotalRotation)}\t{d.Altitude}\t{d.CalculationTimeMs}\t{d.CalculationTimeMs*(ActualMHz/SimulatedMHz)}\t{d.DroppedFrames}\t{d.FeatureUpdate}");
                }
            }

            if (File.Exists(dataPath))
            {
                var result = MessageBox.Show($"Data file exists. Overwrite?\n{dataPath}", "Overwrite?", MessageBoxButtons.OKCancel);
                if (result == DialogResult.Cancel) return;
                File.Delete(dataPath);
            }
            File.Move(tempDataPath, dataPath);

            DrawPathOnStartFrame(tempPathImagePath, endPose);

            if (File.Exists(pathImagePath))
            {
                var result = MessageBox.Show($"Data file exists. Overwrite?\n{pathImagePath}", "Overwrite?", MessageBoxButtons.OKCancel);
                if (result == DialogResult.Cancel) return;
                File.Delete(pathImagePath);
            }
            File.Move(tempPathImagePath, pathImagePath);
        }

        private static void DrawPathOnStartFrame(string tempPathImagePath, Pose endPose)
        {
            using (var result = new Image<Rgb, float>(_startFrame.Bitmap))
            {
                //Actual path
                var actualLineSegment = new LineSegment2DF(
                            CenterPointF,
                            new PointF(endPose.X + CenterPointF.X, endPose.Y + CenterPointF.Y)
                        );
                result.Draw(actualLineSegment, new Rgb(Color.BlueViolet), 3);
                DrawingUtils.DrawPose(result, endPose, CenterPointF, new Rgb(Color.BlueViolet), 3);

                //Estimated path
                var last = frameResults.Count() - 1;
                for (int i = 0; i < frameResults.Count(); i++)
                {
                    if (i == 0 || i % 30 == 0 || i == last)
                    {
                        var frameRes = frameResults[i];
                        var pose = new Pose {
                            AltitudeProportion = frameRes.Altitude,
                            AngleDeg = -(float)GeometricUtils.Rad2Deg(frameRes.TotalRotation),
                            X = -frameRes.TotalTranslation.X,
                            Y = -frameRes.TotalTranslation.Y,
                        };
                        DrawingUtils.DrawPose(result, pose, CenterPointF, new Rgb(0, 255, 0));
                    }
                    if (i < last)
                    {
                        var lineSegment = new LineSegment2DF(
                            new PointF(-frameResults[i].TotalTranslation.X + CenterPointF.X, -frameResults[i].TotalTranslation.Y + CenterPointF.Y),
                            new PointF(-frameResults[i + 1].TotalTranslation.X + CenterPointF.X, -frameResults[i + 1].TotalTranslation.Y + CenterPointF.Y)
                        );
                        result.Draw(lineSegment, new Rgb(255, 0, 0), 1);
                    }
                }
                result.Save(tempPathImagePath);
            }
        }

        private static void Reset()
        {
            _captureSourcePath = GetNextSourcePath();
            if (String.IsNullOrWhiteSpace(_captureSourcePath))
            {
                Application.Exit(); //No more videos
                return;
            }
            capture = new VideoCapture(_captureSourcePath);
            _altitudeProportion = 1f;
            _deltaAltitude = (_altitudeProportion - terminalAltitudeProprotion) / (float)capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount);
            //capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosAviRatio, 0);
            _frameCount = 0;
            
            _keyPoints = null;
            _startFrame = isVideo? capture.QueryFrame() : CvInvoke.Imread(GetFramePath());
            _previousGrayFrame = new Image<Gray, byte>(_startFrame.Bitmap);
            frameResults = new List<FrameResult>();
            IsFirstFrame = true;
        }
    }

    public class KltImageSegment
    {
        public bool IsEdge;
        public Rectangle BoundingBox;
    }

    public class Pose
    {
        public float X;
        public float Y;
        public float AltitudeProportion;
        public float AngleDeg;

        internal Pose Clone()
        {
            return new Pose()
            {
                X = this.X,
                Y = this.Y,
                AltitudeProportion = this.AltitudeProportion,
                AngleDeg = this.AngleDeg,
            };
        }
    }
}
