﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Europa
{
    public class DrawingUtils
    {
        public static void DrawLine(Image<Gray,byte> image, double angle, PointF point)
        {
            double x1, x2, y1, y2;
            var length = Math.Sqrt(Math.Pow(image.Width, 2) + Math.Pow(image.Height, 2)); //Maximum possible length
            x1 = point.X + length * Math.Cos(angle);
            y1 = point.Y + length * Math.Sin(angle);

            x2 = point.X - length * Math.Cos(angle);
            y2 = point.Y - length * Math.Sin(angle);
            var line = new LineSegment2DF() { P1 = new PointF((float)x1, (float)y1), P2 = new PointF((float)x2, (float)y2) };
            image.Draw(line, new Gray(170), 1);
        }

        internal static Image<Gray, byte> DrawHeaderText(string text, Bitmap bitmap, double scale = 1)
        {
            var result = new Image<Gray, byte>(bitmap);
            var x = (int)Math.Round(5 * scale);
            var y = (int)Math.Round(30 * scale);
            result.Draw(text, new Point(x, y), Emgu.CV.CvEnum.FontFace.HersheySimplex, scale, new Gray(90), 6);
            result.Draw(text, new Point(x, y), Emgu.CV.CvEnum.FontFace.HersheySimplex, scale, new Gray(215), 2);
            return result;
        }

        internal static void DrawHeaderText(string text, Image<Gray, byte> binaryImage, double scale = 1)
        {
            var x = (int)Math.Round(5 * scale);
            var y = (int)Math.Round(30 * scale);
            binaryImage.Draw(text, new Point(x, y), Emgu.CV.CvEnum.FontFace.HersheySimplex, scale, new Gray(90), 6);
            binaryImage.Draw(text, new Point(x, y), Emgu.CV.CvEnum.FontFace.HersheySimplex, scale, new Gray(215), 2);
        }

        internal static void DrawHeaderText(string text, Image<Rgb, float> binaryImage, double scale = 1, bool bottom = false)
        {
            var pad = (int)Math.Round(5 * scale);
            var x = pad;
            var y = bottom ? binaryImage.Height - pad : (int)Math.Round(30 * scale);
            binaryImage.Draw(text, new Point(x, y), Emgu.CV.CvEnum.FontFace.HersheySimplex, scale, new Rgb(90,90,90), 6);
            binaryImage.Draw(text, new Point(x, y), Emgu.CV.CvEnum.FontFace.HersheySimplex, scale, new Rgb(215,215,215), 2);
        }

        public static Image<Rgb, float> DrawPoints(Bitmap bitmap, List<PointF> points, Color color)
        {
            var intPoints = points.Select(p => Point.Round(p)).ToList();
            return DrawPoints(bitmap, intPoints, color);
        }

        public static Image<Rgb,float> DrawPoints(Image<Gray, byte> imageGray, List<Point> points, Color color)
        {
            var rgbImage = new Image<Rgb, float>(imageGray.Mat.Bitmap);
            return DrawPoints(rgbImage, points, color);
        }

        public static Image<Rgb, float> DrawPoints(Bitmap bitmap, List<Point> points, Color color)
        {
            var result = new Image<Rgb, float>(bitmap);
            var rect = new Rectangle(1, 1, bitmap.Width - 2, bitmap.Height - 2); //Ignore points on the edge. Yes I am lazy.
            var pointsInRange = points.Where(p => rect.Contains(p)).ToList();
            foreach (var point in pointsInRange)
            {
                result[point.Y, point.X] = new Rgb(color.R, color.G, color.B);
                result[point.Y - 1, point.X - 1] = new Rgb(color.R, color.G, color.B);
                result[point.Y - 1, point.X] = new Rgb(color.R, color.G, color.B);
                result[point.Y, point.X - 1] = new Rgb(color.R, color.G, color.B);
                result[point.Y + 1, point.X + 1] = new Rgb(color.R, color.G, color.B);
                result[point.Y + 1, point.X] = new Rgb(color.R, color.G, color.B);
                result[point.Y, point.X + 1] = new Rgb(color.R, color.G, color.B);
                result[point.Y - 1, point.X + 1] = new Rgb(color.R, color.G, color.B);
                result[point.Y + 1, point.X - 1] = new Rgb(color.R, color.G, color.B);
            }
            return result;
        }

        public static Image<Rgb, float> DrawPoints(Image<Rgb, float> image, List<Point> points, Color color)
        {
            return DrawPoints(image.Bitmap, points, color);
        }

        internal static IImage DrawLines(Bitmap bitmap, List<LineSegment2DF> lines, Color color)
        {
            var result = new Image<Rgb, float>(bitmap);
            var rgb = new Rgb(color.R, color.G, color.B);
            foreach (var line in lines)
            {
                result.Draw(line, rgb, 2);
            }
            return result;
        }

        internal static void DrawLines(Image<Rgb, float> image, List<LineSegment2DF> lines, Color color)
        {
            var rgb = new Rgb(color.R, color.G, color.B);
            foreach (var line in lines)
            {
                image.Draw(line, rgb, 2);
            }
        }

        internal static void DrawPose(Image<Rgb, float> image, Pose p, PointF offset, Rgb color, int thickness = 1)
        {
            float radius = (float)(image.Width / 20.0 * p.AltitudeProportion);
            var center = new PointF((float)p.X + offset.X, (float)p.Y + offset.Y);
            var circle = new CircleF(center, radius);
            image.Draw(circle, color, thickness);
            var angleRad = GeometricUtils.Deg2Rad(p.AngleDeg);
            var line = new LineSegment2DF(center, new PointF(center.X + radius * (float)Math.Cos(angleRad), center.Y + radius * (float)Math.Sin(angleRad)));
            image.Draw(line, new Rgb(0, 0, 255), 2);
        }
    }
}
