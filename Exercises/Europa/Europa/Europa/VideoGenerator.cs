﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Europa
{
    public class VideoGenerator
    {
        public static VideoCapture capture;
        public static VideoWriter writer;
        private static double totalFrames;
        private static ImageViewer viewer;
        private static double FrameDivision = 1;

        public static Stopwatch sw { get; private set; }
        public static int frameCount { get; private set; }
        public static double stddev;
        public static double salt;
        public static double pepper;
        private static bool isStdDev;
        private static bool combineStddevSnP;

        public VideoGenerator(){}

        public void GenerateVideoWithNoise()
        {
            isStdDev = true;
            combineStddevSnP = true;
            salt = 0.025;
            pepper = 0.025;
            //stddev = 0.0458; //normal
            stddev = 0.0916; //double

            var captureSourcePath = "C:\\Users\\grena\\OneDrive\\DTU\\2017 - Haust\\30330 Image Analysis with Microcomputer\\TestVideos\\Pure descent\\Europa rot0 x0 y0 z75-150 t60.mp4";
            capture = new VideoCapture(captureSourcePath);

            string noiseName = "";
            string noiseValue = "";
            if (combineStddevSnP)
            {
                noiseName = "comb_";
                noiseValue = $"std{stddev:0.0000}_s{salt:0.000}p{pepper:0.000}";
            }
            else
            {
                noiseName = isStdDev ? "stddev" : "";
                noiseValue = isStdDev ? $"{stddev:0.0000}" : $"s{salt:0.000}p{pepper:0.000}";
            }
            
            var outputPath = Path.Combine(Path.GetDirectoryName(captureSourcePath), Path.GetFileNameWithoutExtension(captureSourcePath)) + $"_addedNoise_{noiseName}{noiseValue}.avi";
            if (File.Exists(outputPath))
            {
                var result = MessageBox.Show($"File exists do you want to overwrite?\n{outputPath}", "", MessageBoxButtons.OKCancel);
                if(result != DialogResult.OK)
                {
                    return;
                }
                try
                {
                    File.Delete(outputPath);
                }
                catch(Exception ex)
                {
                    MessageBox.Show($"Error deleting file\n{ex.Message}", "", MessageBoxButtons.OK);
                    return;
                }
            }
            sw = new Stopwatch();
            sw.Start();
            var fps = (int)Math.Round(capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps) / FrameDivision);
            var captureSize = new Size(capture.Width, capture.Height);
            //writer = new VideoWriter(outputPath, fps, captureSize, isColor: false);

            writer = new VideoWriter(outputPath,-1, fps, captureSize, isColor:false);
            if (!writer.IsOpened)
            {
                MessageBox.Show("Writer not opened successfully.");
            }

            Application.Idle += ApplicationIdle_Handler;

            totalFrames = capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount);
            viewer = new ImageViewer();

            //Ensure the viewer resizes with the source video
            viewer.ImageBox.SizeMode = PictureBoxSizeMode.AutoSize;
            viewer.AutoSize = true;
            viewer.ShowDialog();
        }

        private static void ApplicationIdle_Handler(object sender, EventArgs e)
        {
            frameCount++;
            var queryFrame = capture.QueryFrame();
            if(queryFrame == null)
            {
                sw.Stop();
                writer.Dispose();
                MessageBox.Show($"Completed after {sw.ElapsedMilliseconds / 1000} [s]");
                Environment.Exit(0); //Quits program
            }
            if (frameCount % FrameDivision == 0) {
                var grayFrame = new Image<Gray, byte>(queryFrame.Bitmap);
                if (combineStddevSnP)
                {
                    grayFrame = ImageUtils.AddWhiteNoiseNormalized(queryFrame.Bitmap, 0.0, stddev);
                    ImageUtils.AddSaltAndPepper(grayFrame, salt, pepper);
                }
                else
                {
                    if (isStdDev)
                    {
                        grayFrame = ImageUtils.AddWhiteNoiseNormalized(queryFrame.Bitmap, 0.0, stddev);
                    }
                    else
                    {
                        ImageUtils.AddSaltAndPepper(grayFrame, salt, pepper);
                    }
                }
                //var noiseResult = ImageUtils.AddNoiseNormalized(queryFrame.Bitmap, 0.0, stddev);
                //var grayFrame = noiseResult.Item1;
                
                writer.Write(grayFrame.Mat);
                //viewer.Image = grayFrame;
                viewer.Text = $"{frameCount / totalFrames * 100} %";
            }
            else
            {
                viewer.Focus(); //Skip frame
            }
        }
    }
}
