﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using Microsoft.Win32;

namespace Europa
{
    public class HardwareUtils
    {
        public static float GetCpuClockSpeedMHz()
        {
            //return (int)Registry.GetValue(@"HKEY_LOCAL_MACHINE\HARDWARE\DESCRIPTION\System\CentralProcessor\0", "~MHz", 0);
            RegistryKey registrykeyHKLM = Registry.LocalMachine;
            string keyPath = @"HARDWARE\DESCRIPTION\System\CentralProcessor\0";
            RegistryKey registrykeyCPU = registrykeyHKLM.OpenSubKey(keyPath, false);
            var MHz = float.Parse(registrykeyCPU.GetValue("~MHz").ToString());
            string ProcessorNameString = (string)registrykeyCPU.GetValue("ProcessorNameString");
            registrykeyCPU.Close();
            registrykeyHKLM.Close();
            return MHz;
        }
    }
}
