﻿using Emgu.CV;
using Emgu.CV.Structure;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Europa
{
    class KltHelper_Old
    {
        public class KltKeyPoint
        {
            public MKeyPoint KeyPoint;
            public MathNet.Numerics.LinearAlgebra.Matrix<double> AffineTransformation;

            public bool IsFirstRun { get; private set; }

            public int X
            {
                get => (int)Math.Round(KeyPoint.Point.X);
            }

            public int Y
            {
                get => (int)Math.Round(KeyPoint.Point.Y);
            }

            public KltKeyPoint(MKeyPoint keyPoint)
            {
                this.KeyPoint = keyPoint;
                this.IsFirstRun = true;
                this.AffineTransformation = KltHelper_Old.CreateAffineMatrix(1, 0, 0, 1, 0, 0); //No warping
            }

            public double DistanceTo(PointF point)
            {
                return Math.Sqrt(Math.Pow(point.X - KeyPoint.Point.X, 2) + Math.Pow(point.Y - KeyPoint.Point.Y, 2));
            }

            public void UpdateAffineTransformation(Vector<double> deltaP)
            {
                AffineTransformation[0, 0] += deltaP[0]; //a1
                AffineTransformation[0, 1] += deltaP[1]; //a2
                AffineTransformation[0, 2] += deltaP[2]; //b1
                AffineTransformation[1, 0] += deltaP[3]; //a3
                AffineTransformation[1, 1] += deltaP[4]; //a4
                AffineTransformation[1, 2] += deltaP[5]; //b2

                var newPos = AffineTransformation * CreateVector.DenseOfArray<double>(new double[] { X, Y, 1 });
                KeyPoint.Point = new PointF((float)newPos[0], (float)newPos[1]);
            }

            public bool CheckDeltaP(Vector<double> deltaP, double tolerance = 0.1)
            {
                if (!IsFirstRun) return true;
                var deltaCopy = deltaP.Clone();

                deltaCopy[0] -= AffineTransformation[0, 0]; //a1
                deltaCopy[1] -= AffineTransformation[0, 1]; //a2
                deltaCopy[2] -= AffineTransformation[0, 2]; //b1
                deltaCopy[3] -= AffineTransformation[1, 0]; //a3
                deltaCopy[4] -= AffineTransformation[1, 1]; //a4
                deltaCopy[5] -= AffineTransformation[1, 2]; //b2
                var norm = deltaP.Norm(2);
                return norm < tolerance;
            }
        }

        private static Vector<double> WarpPixel(Vector<double> positionXYZ, MathNet.Numerics.LinearAlgebra.Matrix<double> affineTransformation)
        {
            return affineTransformation * positionXYZ;
        }

        public static Image<Gray, byte> TransformImageWindow(Image<Gray, byte> image, Rectangle window, MathNet.Numerics.LinearAlgebra.Matrix<double> affineTransformation)
        {
            var result = new Image<Gray, byte>(window.Width, window.Height);
            var maxX = window.Location.X + window.Width;
            var maxY = window.Location.Y + window.Height;
            for (int x = 0; x < window.Width; x++)
            {
                for(int y = 0; y < window.Height; y++)
                {
                    var position = CreateVector.DenseOfArray<double>(new double[] {x - window.Width/2, y - window.Height/2, 0});
                    var newIndex = WarpPixel(position, affineTransformation);
                    var newX = window.Location.X + (window.Width - 1) / 2 + (int)Math.Round(newIndex[0]);
                    var newY = window.Location.Y + (window.Height -1) / 2 + (int)Math.Round(newIndex[1]);
                    try
                    {
                        result[y, x] = image[newY, newX];
                    }
                    catch
                    {
                        result[y, x] = new Gray(0); 
                    }
                }
            }
            return result;
        }

        internal static MathNet.Numerics.LinearAlgebra.Matrix<double> CreateAffineMatrix(double a1, double a2, double a3, double a4, double b1, double b2)
        {
            var input = new double[2][];
            input[0] = new double[] { a1, a2, b1 };
            input[1] = new double[] { a3, a4, b2 };
            return CreateMatrix.DenseOfRowArrays(input);
        }

        internal static MathNet.Numerics.LinearAlgebra.Matrix<double> Jacobian(int x, int y)
        {
            var input = new double[2][];
            input[0] = new double[] { 1, 0, x, y, 0, 0 };
            input[1] = new double[] { 0, 1, 0, 0, x, y };
            return CreateMatrix.DenseOfRowArrays(input);
        }

        internal static Vector<double> CalcPixelGradient(int xGlob, int yGlob, Image<Gray, byte> image)
        {
            var gradX = -image[yGlob, xGlob - 1].Intensity + image[yGlob, xGlob + 1].Intensity; // - left + right
            var gradY = -image[yGlob - 1, xGlob].Intensity + image[yGlob + 1, xGlob].Intensity;// - up + down
            return CreateVector.DenseOfArray(new double[] { gradX, gradY });
        }

        internal static MathNet.Numerics.LinearAlgebra.Matrix<double> PartialInverseHessian(Vector<double> steepestDescent)
        {
            var columns = new double[steepestDescent.Count][];

            for(var i = 0; i < 6; i ++)
            {
                var row = new double[6];
                for(var j = 0; j < 6; j++)
                {
                    row[j] = steepestDescent[i] * steepestDescent[j];
                }
                columns[i] = row;
            }

            return CreateMatrix.DenseOfColumnArrays<double>(columns);
        }

    }
}
