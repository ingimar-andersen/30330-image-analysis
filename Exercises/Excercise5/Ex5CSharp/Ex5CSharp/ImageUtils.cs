﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using System.Drawing;

namespace Ex5CSharp
{
    public class ImageUtils
    {
        public class BinaryImageResult : IDisposable
        {
            public Image<Gray, byte> BinaryImage;
            public PointF CenterOfMass;

            public void Dispose()
            {
                BinaryImage.Dispose();
            }
        }

        public static Image<Bgr,Byte> GenerateHistogram(Mat image, int histWidth = 1044, int histHeight = 720) 
        {
            using (Image<Gray, Byte> img = image.ToImage<Gray, Byte>())
            {
                Array channel; // pre-allocated array holding image data for the color channel or the grayscale image.

                channel = img.Data;

                int[] histogram = new int[Byte.MaxValue + 1]; // histogram array - remember to set to zero initially
                int width = image.Width;
                int height = image.Height;
                int k = Byte.MaxValue;
                while (k-- > 0)
                {
                    histogram[k] = 0; // reset histogram entry
                }
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        var value = img[i, j];
                        histogram[(int)value.Intensity] += 1;
                    }
                }

                // create the output image
                Image<Bgr, Byte> histImage = new Image<Bgr, Byte>(histWidth, histHeight);

                var backgrRect = new Rectangle() { X = 0, Y = 0, Width = histWidth, Height = histHeight };
                histImage.Draw(backgrRect, new Bgr(Color.LightGray), -1); // Thickness < 1 is fill

                int paddingTop = 10, paddingBottom = 10, paddingLeft = 10, paddingRight = 10;

                //Find max value for scaling


                int maxValue = histogram.Max();
                var indexAtMax = histogram.ToList().IndexOf(maxValue);

                LineSegment2DF tempLine = new LineSegment2DF();

                int columnSpacing = (int)Math.Round((histWidth - paddingLeft - paddingRight) / (float)histogram.Length);
                //Draw each column of histogram
                for (int i = 0; i < histogram.Length; i++)
                {
                    int x = (i + 1) * columnSpacing;
                    if (i == 0)
                    {
                        x += paddingLeft;
                    }
                    //Bottom
                    tempLine.P1 = new PointF(x, histHeight - paddingBottom);

                    //Top
                    float topY = (histHeight - paddingBottom - paddingTop) * (1 - (float)histogram[i] / (float)maxValue) + paddingBottom;
                    tempLine.P2 = new PointF(x, topY);

                    histImage.Draw(tempLine, new Bgr(Color.ForestGreen), columnSpacing / 2, Emgu.CV.CvEnum.LineType.AntiAlias);
                }

                Console.WriteLine(String.Format("Frame settings:\n Width: {0}\n Height: {1}\n", histImage.Width, histImage.Height));
                Console.WriteLine(String.Format("Max value: {0} at index {1}", maxValue, indexAtMax));

                return histImage;
            }
        }

        public static Point FindFirstEdge(Image<Gray, byte> binaryImage, int threshold)
        {
            return FindFirstEdge(binaryImage, threshold, new Point(0, 0));
        }

        public static Point FindFirstEdge(Image<Gray, byte> binaryImage, int threshold, Point point)
        {
            for(int x = point.X; x < binaryImage.Width; x++)
            {
                for(int y = point.Y; y < binaryImage.Height; y++)
                {
                    //If black, check if it has white neighbours
                    if (binaryImage[y, x].Intensity < threshold)
                    {
                        if(HasNeighboursOverThreshold(binaryImage, x, y, threshold))
                        {
                            return new Point(x, y);
                        }
                    }
                }
            }
            //No edge found
            return Point.Empty;
        }

        private static bool HasNeighboursOverThreshold(Image<Gray, byte> binaryImage, int x, int y, int threshold)
        {
            for (int i = x - 1; i <= x + 1; i++)
            {
                for (int j = y - 1; j <= y + 1; j++)
                {
                    //Do not check center
                    if (i == x && j == y)
                    {
                        continue;
                    }
                    var xCoordinate = i;
                    var yCoordinate = j;

                    //Keep coordinites within image matrix
                    if (xCoordinate < 0)
                    {
                        continue;
                    }
                    else if (xCoordinate >= binaryImage.Width)
                    {
                        continue;
                    }
                    else if (yCoordinate < 0)
                    {
                        continue;
                    }
                    else if (yCoordinate >= binaryImage.Height)
                    {
                        continue;
                    }
                    if(binaryImage[yCoordinate, xCoordinate].Intensity > threshold)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static Image<Gray, byte> CreateBinaryImage(Mat image, int threshold){
            var binaryImage = image.Clone().ToImage<Gray, byte>();
            double x = 0, y = 0;
            for (int height = 0; height < binaryImage.Height; height++)
            {
                for (int width = 0; width < binaryImage.Width; width++)
                {
                    var value = binaryImage[height, width];
                    if (value.Intensity < threshold)
                    {
                        value.MCvScalar = new MCvScalar(byte.MinValue);
                        x += width;
                        y += height;
                    }
                    else
                    {
                        value.MCvScalar = new MCvScalar(byte.MaxValue);
                    }
                    binaryImage[height, width] = value;
                }
            }
            return binaryImage;
        }

        public static BinaryImageResult CreateBinaryImageWithCenterOfMass(Mat image, int threshold)
        {
            var binaryImage = image.Clone().ToImage<Gray, byte>();
            int nonZeroCount = 0;
            double x = 0, y = 0;
            for (int height = 0; height < binaryImage.Height; height++)
            {
                for (int width = 0; width < binaryImage.Width; width++)
                {
                    var value = binaryImage[height, width];
                    if (value.Intensity < threshold)
                    {
                        value.MCvScalar = new MCvScalar(byte.MinValue);
                        x += width;
                        y += height;
                        nonZeroCount++;
                        //value.Intensity = byte.MaxValue;
                    }
                    else
                    {
                        value.MCvScalar = new MCvScalar(byte.MaxValue);
                        //value.Intensity = byte.MinValue;
                    }
                    binaryImage[height, width] = value;
                }
            }
            PointF centerOfMass = new PointF();
            if (nonZeroCount > 0) //Division by zero
            {
                centerOfMass = new PointF() { X = (float)Math.Round(x / nonZeroCount), Y = (float)Math.Round(y / nonZeroCount) };

                //Testing
                //centerOfMass = new PointF(centerOfMass.X - 30, centerOfMass.Y + 30);

                var size = new SizeF() { Height = binaryImage.Height / 15, Width = binaryImage.Height / 15 };
                var cross = new Cross2DF() { Center = centerOfMass, Size = size };

                binaryImage.Draw(cross, new Gray(128), 2);

                var principalAngle = ImageUtils.CalcPrincipalAngle(binaryImage, centerOfMass);
                DrawingUtils.DrawLine(binaryImage, principalAngle, centerOfMass);
            }
            var result = new BinaryImageResult();
            result.BinaryImage = binaryImage;
            result.CenterOfMass = centerOfMass;
            return result;
        }

        public static double CalcPrincipalAngle(Image<Gray,byte> binaryImage, PointF centerOfMass, bool inDeg = false)
        {
            var centralMoment00 = CalcCentralMoment(binaryImage, 0, 0, centerOfMass);
            var redCentralMoment11 = CalcCentralMoment(binaryImage, 1, 1, centerOfMass) / centralMoment00;
            var redCentralMoment20 = CalcCentralMoment(binaryImage, 2, 0, centerOfMass) / centralMoment00;
            var redCentralMoment02 = CalcCentralMoment(binaryImage, 0, 2, centerOfMass) / centralMoment00;

            var angle = Math.Atan2(2 * redCentralMoment11, (redCentralMoment20 - redCentralMoment02)) / 2; 
            return angle;
        }

        private static double CalcCentralMoment(Image<Gray, byte> binaryImage, int xOrder, int yOrder, PointF centerOfMass)
        {
            double centralMoment = 0;
            for (int y = 0; y < binaryImage.Height; y++)
            {
                for(int x = 0; x < binaryImage.Width; x++)
                {
                    var value = binaryImage[y, x];
                    centralMoment += Math.Pow(x - centerOfMass.X, xOrder) * Math.Pow(y - centerOfMass.Y, yOrder)*Math.Abs(value.Intensity-Byte.MaxValue);
                }
            }
            return centralMoment;
        }

        public static double CalcHuInvariantMoments(Image<Gray, byte> image, PointF centerOfMass)
        {
            return CalcInvariantMoment(image, 2, 0, centerOfMass) + CalcInvariantMoment(image, 0, 2, centerOfMass);
        }

        private static double CalcInvariantMoment(Image<Gray, byte> image, int xOrder, int yOrder, PointF centerOfMass)
        {
            var centralMoment = CalcCentralMoment(image, xOrder, yOrder, centerOfMass);
            var zeroCentralMoment = CalcCentralMoment(image, 0, 0, centerOfMass);

            return centralMoment / Math.Pow(zeroCentralMoment, 1 + (xOrder + yOrder) / 2.0);
        }

        public static List<Point> FindContour(Image<Gray, byte> binaryImage, Point pos)
        {
            var MAX_RAND = 10000;
            int local_tresh, draw_type;
            local_tresh = 128;
            draw_type = 0;
            int count = 0;
            Point newpos = pos; // pos equals the starting position in the image ( =y* Width+x)
            var picRect = new Rectangle(0, 0, binaryImage.Width, binaryImage.Height);
            var contour = new List<Point>();
            var prevPos = pos;
            while (picRect.Contains(newpos))
            {
                contour.Add(newpos);
                count++;
                draw_type = (draw_type + 6) % 8; // Select next search direction
                switch (draw_type)
                {
                    case 0:
                        {
                            if (binaryImage[newpos.Y, newpos.X + 1].Intensity < local_tresh) { newpos.X += 1; draw_type = 0; break; }
                            goto case 1;
                        }
                    case 1:
                        {
                            if (binaryImage[newpos.Y + 1, newpos.X + 1].Intensity < local_tresh) { newpos.Y += 1; newpos.X += 1; draw_type = 1; break; }
                            goto case 2;
                        }
                    case 2:
                        {
                            if (binaryImage[newpos.Y + 1, newpos.X].Intensity < local_tresh) { newpos.Y += 1; draw_type = 2; break; }
                            goto case 3;
                        }
                    case 3:
                        {
                            if (binaryImage[newpos.Y + 1, newpos.X - 1].Intensity < local_tresh) { newpos.Y += 1; newpos.X -= 1; draw_type = 3; break; }
                            goto case 4;
                        }
                    case 4:
                        {
                            if (binaryImage[newpos.Y, newpos.X - 1].Intensity < local_tresh) { newpos.X -= 1; draw_type = 4; break; }
                            goto case 5;
                        }
                    case 5:
                        {
                            if (binaryImage[newpos.Y - 1, newpos.X - 1].Intensity < local_tresh) { newpos.Y -= 1; newpos.X -= 1; draw_type = 5; break; }
                            goto case 6;
                        }
                    case 6:
                        {
                            if (binaryImage[newpos.Y - 1, newpos.X].Intensity < local_tresh) { newpos.Y -= 1; draw_type = 6; break; }
                            goto case 7;
                        }
                    case 7:
                        {
                            if (binaryImage[newpos.Y - 1, newpos.X + 1].Intensity < local_tresh) { newpos.Y -= 1; newpos.X += 1; draw_type = 7; break; }
                            goto case 8;
                        }
                    case 8:
                        {
                            if (binaryImage[newpos.Y, newpos.X + 1].Intensity < local_tresh) { newpos.X += 1; draw_type = 0; break; }
                            goto case 9;
                        }
                    case 9:
                        {
                            if (binaryImage[newpos.Y + 1, newpos.X + 1].Intensity < local_tresh) { newpos.Y += 1; newpos.X += 1; draw_type = 1; break; }
                            goto case 10;
                        }
                    case 10:
                        {
                            if (binaryImage[newpos.Y + 1, newpos.X].Intensity < local_tresh) { newpos.Y += 1; draw_type = 2; break; }
                            goto case 11;
                        }
                    case 11:
                        {
                            if (binaryImage[newpos.Y + 1, newpos.X - 1].Intensity < local_tresh) { newpos.Y += 1; newpos.X -= 1; draw_type = 3; break;}
                            goto case 12;
                        }
                    case 12:
                        {
                            if (binaryImage[newpos.Y, newpos.X - 1].Intensity < local_tresh) { newpos.X -= 1; draw_type = 4; break; }
                            goto case 13;
                        }
                    case 13:
                        {
                            if (binaryImage[newpos.Y - 1, newpos.X - 1].Intensity < local_tresh) { newpos.Y -= 1; newpos.X -= 1; draw_type = 5; break; }
                            goto case 14;
                        }
                    case 14:
                        {
                            if (binaryImage[newpos.Y - 1, newpos.X].Intensity < local_tresh) { newpos.Y -= 1; draw_type = 6; break; }
                            break;
                        }
                }

                // If we are back at the beginning, we declare success
                if (newpos == pos)
                    break;
                // Abort if the contour is too complex.
                if (count >= MAX_RAND)
                {
                    break;
                }
            }
            return contour;
        }

        public static Dictionary<Point, Correspondance> FindCorrespondances(Image<Gray, byte> searchImage, Image<Gray, byte> comparisonImage, List<Point> comparisonPoints, int n)
        {
            //Init best matches
            var bestMatches = new Dictionary<Point, Correspondance>();

            //Cut out comparison windows
            var comparisonWindows = new List<Tuple<Image<Gray, byte>, Point>>();
            var size = 2 * n + 1;
            foreach(var point in comparisonPoints)
            {
                try
                {
                    var comparisonRect = new Rectangle(point.X - n, point.Y - n, size, size);
                    var window = comparisonImage.GetSubRect(comparisonRect);
                    comparisonWindows.Add(new Tuple<Image<Gray, byte>, Point>(window, point));
                    bestMatches.Add(point, new Correspondance(point, n));
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                
            }

            //Only compare parts of the image that are equal in size with the comparison matrix.
            var offset = n;
            var xMax = searchImage.Width - offset;
            var xMin = offset;
            var yMax = searchImage.Height - offset;
            var yMin = offset;

            var count = 0;
            for (int x = xMin; x < xMax; x++)
            {
                for(int y = yMin; y < yMax; y++)
                {
                    var searchCandidate = searchImage.GetSubRect(new Rectangle(x - xMin, y - yMin, size, size));
                    foreach (var win in comparisonWindows)
                    {
                        count++;
                        double match = CalcCorrelationMatch(win.Item1, searchCandidate);
                        //Update if better
                        var point = win.Item2;
                        if (match < bestMatches[point].Match)
                        {
                            bestMatches[point].UpdateMatch(x, y, match);
                        }
                    }
                }
                var percentageComplete = (x - xMin + 1) / (double)(xMax - xMin)* 100;
                Console.Write(String.Format("\r{0:0.00} %   ", percentageComplete));
            }

            Console.WriteLine(count);

            return bestMatches;
        }

        private static double CalcCorrelationMatch(Image<Gray, byte> comparison, Image<Gray, byte> searchCandidate)
        {
            double errorSum = 0;
            for (int x = 0; x < comparison.Width; x++)
            {
                for(int y = 0; y < comparison.Height; y++)
                {
                    var error = Math.Abs(comparison[y, x].Intensity - searchCandidate[y, x].Intensity);
                    errorSum += error;// * error;
                }
            }
            return errorSum; // / (comparison.Width * comparison.Height);
        }
    }

    public class Correspondance
    {
        public int? X;
        public int? Y;
        public readonly Point SourcePoint;
        public readonly int N;

        public double Match;

        public Correspondance(Point sourcePoint, int n) {
            this.X = null;
            this.Y = null;
            this.SourcePoint = sourcePoint;
            this.Match = double.MaxValue;
            this.N = n;
        }

        internal void UpdateMatch(int x, int y, double match)
        {
            this.X = x;
            this.Y = y;
            this.Match = match;
        }
    }
}
