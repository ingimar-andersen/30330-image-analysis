﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using System.Drawing;
using System.Windows.Forms;

namespace Excercise1CSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Mat image;
                using (ImageViewer viewer = new ImageViewer())
                {
                    var capture = new VideoCapture();
                    //capture.ImageGrabbed += ProcessFrame;
                    //The first images are overexposed. 
                    for(int i = 0; i < 15; i++)
                    {
                        capture.QueryFrame();
                    }
                    image = capture.QueryFrame();
                    viewer.Image = image; //draw the image obtained from camera
                    viewer.Show(); //show the image viewer
                    var hist = ImageUtils.GenerateHistogram(image);
                    using (var histViewer = new ImageViewer(hist)) //image set in constructor as a work-around for now showing scroll bars.
                    {
                        histViewer.ShowDialog();
                    }
                }
                //Application.Idle += new EventHandler(delegate (object sender, EventArgs e)
                //{  //run this until application closed (close button click on image viewer)
                //    viewer.Image = capture.QueryFrame(); //draw the image obtained from camera
                //});
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }

    }
}
