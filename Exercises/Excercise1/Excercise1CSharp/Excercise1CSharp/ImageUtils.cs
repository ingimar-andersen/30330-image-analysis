﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using System.Drawing;

namespace Excercise1CSharp
{
    public class ImageUtils
    {

        public static Image<Bgr,Byte> GenerateHistogram(Mat image, int histWidth = 1044, int histHeight = 720) 
        {
            Image<Gray, Byte> img = image.ToImage<Gray, Byte>();

            Array channel; // pre-allocated array holding image data for the color channel or the grayscale image.

            channel = img.Data;

            int[] histogram = new int[Byte.MaxValue]; // histogram array - remember to set to zero initially
            int width = image.Width; // say, 320
            int height = image.Height; // say, 240
            int k = Byte.MaxValue;
            while (k-- > 0)
            {
                histogram[k] = 0; // reset histogram entry
            }
            for (int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    var value = img[i,j];
                    histogram[(int)value.Intensity] += 1;
                }
            }

            // create the output image
            Image<Bgr, Byte> histImage = new Image<Bgr, Byte>(histWidth, histHeight);

            var backgrRect = new Rectangle() { X = 0, Y = 0, Width = histWidth, Height = histHeight };
            histImage.Draw(backgrRect, new Bgr(Color.LightGray), -1); // Thickness < 1 is fill
          
            int paddingTop = 10, paddingBottom = 10, paddingLeft = 10, paddingRight = 10;

            //Find max value for scaling


            int maxValue = histogram.Max();

            LineSegment2DF tempLine = new LineSegment2DF();

            int columnSpacing = (int) Math.Round((histWidth - paddingLeft - paddingRight) / (float)histogram.Length);
            //Draw each column of histogram
            for (int i = 0; i < histogram.Length; i++)
            {
                int x = (i + 1) * columnSpacing;
                if (i == 0)
                {
                    x += paddingLeft;
                }
                //Bottom
                tempLine.P1 = new PointF(x, histHeight - paddingBottom);

                //Top
                float topY = (histHeight - paddingBottom - paddingTop) * (1 - (float)histogram[i] / (float)maxValue) + paddingBottom;
                tempLine.P2 = new PointF(x, topY);

                histImage.Draw(tempLine, new Bgr(Color.ForestGreen), columnSpacing/2,Emgu.CV.CvEnum.LineType.AntiAlias);
            }

            Console.WriteLine("Frame settings:\n Width: %d\n Height: %d\n", histImage.Width, histImage.Height);

            return histImage;
        }
    }
}
