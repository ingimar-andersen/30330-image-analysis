#ifndef IMAGEUTILS_H_
#define IMAGEUTILS_H_

#include <stdio.h>
#include <iostream>
#include <string>
#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <conio.h>
#include <sstream>

void printImageInfo(IplImage * img, std::string name);
IplImage* generateHistogram(IplImage* inputImage, int imageWidth, int imageHeight);

#endif