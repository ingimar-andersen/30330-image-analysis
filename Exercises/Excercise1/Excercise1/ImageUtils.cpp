#include "ImageUtils.h"

#include <stdio.h>
#include <iostream>
#include <string>
#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <conio.h>
#include <sstream>

void printImageInfo(IplImage * img, std::string name){
	printf("\nImageInfo:\n\n");

	printf("\tName:       \t%s\n", name.c_str());
	printf("\tImage size: \t%d bytes\n", img->imageSize);
	printf("\tN size:     \t%d\n", img->nSize);

	std::string dataorder = img->dataOrder == 0 ? "interleaved color channels" : "seperate color channels";
	printf("\tDataorder   \t%s\n", dataorder.c_str());
	
	printf("\tWidth       \t%d\n", img->width);
	printf("\tHeight      \t%d\n", img->height);
	printf("Pixel depth   \t%d\n", img->depth);
	
	printf("\n");
}

IplImage* generateHistogram(IplImage* inputImage, int imageWidth, int imageHeight) {

	char *channel; // pre-allocated array holding image data for the color channel or the grayscale image.
	channel = inputImage->imageData;

	unsigned char value = 0; // index value for the histogram (not really needed)
	int histogram[256]; // histogram array - remember to set to zero initially
	int width = inputImage->width; // say, 320
	int height = inputImage->height; // say, 240
	int k = 256;
	while (k-- > 0) {
		histogram[k] = 0; // reset histogram entry
	}
	for (int i = 0; i<width*height; i++)
	{
		value = channel[i];
		histogram[value] += 1;
	}
	
	// create the output image
	IplImage* hist = cvCreateImage(cvSize(imageWidth, imageHeight), 8, 3);
	cvZero(hist);

	CvPoint pt1, pt2;
	pt1.x = 0;
	pt1.y = 0;

	pt2.x = imageWidth;
	pt2.y = imageHeight;
	CvScalar backgroundColor = CV_RGB(200, 200, 200); //light grey
	cvRectangle(hist, pt1, pt2, backgroundColor, -1);

	CvScalar columnColor = CV_RGB(0, 119, 51); //Dark green

	CvPoint tempBottom, tempTop;
	int paddingTop = 10, paddingBottom = 10, paddingLeft = 10, paddingRight = 10;
	int histogramLength = sizeof(histogram) / sizeof(*histogram);

	//Find max value for scaling
	int maxValue = 0;
	for (int i = 0; i < histogramLength; i++) {
		int value = histogram[i];
		if (value > maxValue) {
			maxValue = value;
		}
	}

	int columnSpacing = (imageWidth - paddingLeft - paddingRight) / (float)histogramLength;
	//Draw each column of histogram
	for (int i = 0; i < histogramLength; i++) {
		int x = (i + 1)* columnSpacing;
		if (i == 0) {
			x += paddingLeft;
		}

		tempBottom.x = x;
		tempBottom.y = imageHeight - paddingBottom;

		tempTop.x = x;
		tempTop.y = (imageHeight - paddingBottom - paddingTop) * (1 - (float)histogram[i] / (float)maxValue) + paddingBottom;

		cvLine(hist, tempBottom, tempTop, columnColor, columnSpacing / 2);
	}

	printf("Frame settings:\n Width: %d\n Height: %d\n", hist->width, hist->height);

	return hist;
}