#include "PathUtil.h"

#include <stdio.h>
#include <iostream>
#include <Windows.h>
#include <string>
#include <conio.h>
#include <sstream>

using std::string;

string getWorkingFolder() {
	wchar_t buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, _MAX_PATH);
	std::wstring ws(buffer);
	// your new String
	string str(ws.begin(), ws.end());
	string appPath = str.substr(0, str.find_last_of("\\/"));
	appPath = appPath.substr(0, appPath.find_last_of("\\/")); //Remove Debug folder
	return appPath;
}