#include <stdio.h>
#include <iostream>
#include <Windows.h>
#include <string>
#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <conio.h>
#include <sstream>
#include <atlstr.h>

#include "ImageUtils.h"
#include "PathUtil.h"

using namespace std;

static const int HIST_WIDTH = 1044;
static const int HIST_HEIGHT = 720;

int main(int argc, char *argv[]){
	
	string appPath = getWorkingFolder();

	string input;
	printf("The working folder is %s\n\n", appPath.c_str());
	printf("Enter relative file path: \n");
	getline(std::cin, input);

	stringstream ss;
	ss << appPath << "\\" << input;
	string imagePath = ss.str();

	char absolutePath[1024];
	strcpy(absolutePath, imagePath.c_str());
	IplImage* img = cvLoadImage(absolutePath);
	IplImage* imgCopy = 0;

	printImageInfo(img, input);

	//Create window
	const char* windowName = input.c_str(); //Used as identifier by cvShowImage
	cvNamedWindow(windowName, 0);

	//for (;;)
	//{
	//	/*if (!cvGrabFrame(capture))
	//		break;*/
	//	//frame = cvRetrieveFrame(capture);
	//	if (!img)
	//		break;
	//	if (!imgCopy)
	//	{
	//		printf("Frame settings:\n Width: %d\n Height: %d\n", img->width, img->height);
	//		imgCopy = cvCreateImage(cvSize(img->width, img->height), IPL_DEPTH_8U, img->nChannels);
	//		cvResizeWindow(windowName, img->width, img->height);
	//	}
	//	if (img->origin == IPL_ORIGIN_TL)
	//		cvCopy(img, imgCopy, 0);
	//	else
	//		cvFlip(img, imgCopy, 0);
	//	cvShowImage(windowName, imgCopy);
	//	if (cvWaitKey(5)>0)
	//		break;
	//}

	IplImage* hist = generateHistogram(img, HIST_WIDTH, HIST_HEIGHT);

	cvResizeWindow(windowName, hist->width, hist->height);

	cvShowImage(windowName, hist);
	for (;;)
	{
		/*if (!cvGrabFrame(capture))
		break;*/
		//frame = cvRetrieveFrame(capture);
		/*if (hist->origin == IPL_ORIGIN_TL)
			cvCopy(hist, histCopy, 0);
		else
			cvFlip(hist, histCopy, 0);*/
		
		if (cvWaitKey(5)>0)
			break;
	}

	return 0;
}

